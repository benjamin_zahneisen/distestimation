# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import numpy as np
import nibabel as nib
import cv2
import matplotlib.pyplot as plt
from matplotlib.pyplot import imshow
import keras.backend as K


#import the flow model
from utils import getDatasets,loadNiftiDatasets,loadFSLcorrectedDatasets,_extractTrainingData,_resizeUpDownDatasets,vol2mosaic,loadNiftiVol
from distEstimatorNet.preprocessInput import preprocessInputData
from distEstimatorNet.distEstimator import UpDownFlow
from utils import displayResults,vol2mosaic,displayFSLResults


#base_folder = '/home/bzahneisen/upDownData'
base_folder_test = '/Users/bzahneisen/Dropbox/distortionEstimation/updowndata/upDownDataTest'
datasetsInfo = getDatasets(base_folder_test)

upDownDatasetsTest = loadNiftiDatasets(base_folder_test,getDatasets(base_folder_test))
upDownDatasetsTest_N128 = _resizeUpDownDatasets(upDownDatasetsTest,(128,128))
testData = _extractTrainingData(upDownDatasetsTest_N128,normalize=True)

fsl_upDownDatasets = loadFSLcorrectedDatasets(base_folder_test,datasetsInfo)
fsl_upDownDatasets_N128 = _resizeUpDownDatasets(fsl_upDownDatasets,(128,128))
fsl_testData = _extractTrainingData(fsl_upDownDatasets_N128,normalize=True,removeEmptySlices=False)


######prepare data
n_test_full,n_testN64,n_testN32 = preprocessInputData(testData)
inputTest = [n_test_full,n_testN64,n_testN32]

n_fsl_full,n_fslN64,n_fslN32 = preprocessInputData(fsl_testData)
n_fsl_full[np.isnan(n_fsl_full)]=0
inputFSL = [n_fsl_full,n_fslN64,n_fslN32]



###############################
#   Construct the model and load weights
###############################
updownflow = UpDownFlow(architecture = 'SimpleFlowNet_v4',input_size=128,batchNormFlag=True)
folderName = 'v4_fullTraining_0611_lmba1_m4428'
updownflow.load('./'+folderName+'/run_4.h5')

# predict 
prediction = updownflow.predict(inputTest)
out,out64,out32,vdm,vdm64,vdm32 = prediction


## trim model ###
json_model_str = updownflow.feature_extractor.model.to_json()
json_new_model_str = json_utils.trim_model(json_model_str)

with open('data.txt', 'w') as f:
    json.dump(json_new_model_str, f)

for n in range(len(json_model_str)):
    if (json_model_str[0:n] == json_new_model_str[0:n] ) == False:
        print(n)
        break
        

#load model
from keras.models import model_from_json
with open('data.txt') as json_data:
    json_string2 = json.load(json_data)
    json_data.close()
model = model_from_json(json_new_model_str) 
model.load_weights('./v4_fullTraining/v4_withNewTrainData_0424.h5')
vdmNeu=model.predict(n_test_full,batch_size=32,verbose=1)
imshow(vdmNeu[25,:,:,0])
####


#############
# calculate FSL loss (does not use regularization)
tensor1 = K.constant(n_fsl_full)
fslLoss = K.get_value(updownflow.image_loss(tensor1,tensor1)) # = 0.077441

tensor2 = K.constant(out)
predictedLoss = K.get_value(updownflow.image_loss(tensor2,tensor2))


#load loss
#folderName = 'v4_fullTraining_0603_lmba0.2_m4428'
loss = np.load('./'+folderName+'/loss.npy')
valLoss = np.load('./'+folderName+'/valLoss.npy')

# summarize history for loss
startIdx = 0
plt.plot(loss[startIdx:])
plt.plot(valLoss[startIdx:])
plt.title('Mean squared error loss, lambda='+str(1))
plt.ylabel('MSE')
plt.xlabel('# of epochs')

if not fslLoss==None:
    plt.axhline(fslLoss,linewidth=1.5,color='black',linestyle='dashed')
plt.legend(['training loss', 'test loss','topup loss'], loc='upper right')
plt.ylim((0.02,0.25))        
plt.show()
plt.savefig('./'+folderName+'/loss.png')
##########3

#104 ,80,75
t=180
displayFSLResults(n_fsl_full,out,n_test_full,t)

t= 105
displayResults(prediction,inputTest,t)


#export selected slices
m_out = np.mean(out,axis=3)
m_fsl = np.mean(n_fsl_full,axis=3)
diffOut = out[:,:,:,0] - out[:,:,:,1]
diffFSL = n_fsl_full[:,:,:,1] - n_fsl_full[:,:,:,0]
folder_name = '../figures/Testdata/'

fig,ax = plt.subplots(1);ax.imshow(np.transpose(vdm[105,:,::-1,0]),vmin=-6,vmax=6);ax.axes.axis('off')
fig.savefig(folder_name+str(t)+'_in0',bbox_inches='tight')

t=104
tmp = np.transpose(n_test_full[t,:,::-1,0]);tmp = cv2.GaussianBlur(tmp,(3,3),1)
tmp2 = np.transpose(n_test_full[t,:,::-1,1]);tmp2 = cv2.GaussianBlur(tmp2,(3,3),1)
fig,ax = plt.subplots(1);ax.imshow(np.transpose(n_test_full[t,:,::-1,0]),cmap='gray',vmin=0,vmax=5);ax.axes.axis('off')
ax.contour(tmp,levels=[1],colors='b',linewidth=0.5,alpha=0.5)
ax.contour(tmp2,levels=[1],colors='r')
fig,ax = plt.subplots(1);ax.imshow(np.transpose(n_test_full[t,:,::-1,0]),cmap='gray',vmin=0,vmax=5);ax.axes.axis('off')
fig.savefig(folder_name+str(t)+'_in0',bbox_inches='tight')
fig,ax = plt.subplots(1);ax.imshow(np.transpose(n_test_full[t,:,::-1,1]),cmap='gray',vmin=0,vmax=5);ax.axes.axis('off')
fig.savefig(folder_name+str(t)+'_in1',bbox_inches='tight')
fig,ax = plt.subplots(1);ax.imshow(np.transpose(m_out[t,:,::-1]),cmap='gray',vmin=0,vmax=5);ax.axes.axis('off')
fig.savefig(folder_name+str(t)+'_mout',bbox_inches='tight')
fig,ax = plt.subplots(1);ax.imshow(np.transpose(diffOut[t,:,::-1]),vmin=-2,vmax=2);ax.axes.axis('off')
fig.savefig(folder_name+str(t)+'_moutDiff',bbox_inches='tight')
fig,ax = plt.subplots(1);ax.imshow(np.transpose(m_fsl[t,:,::-1]),cmap='gray',vmin=0,vmax=5);ax.axes.axis('off')
fig.savefig(folder_name+str(t)+'_mfsl',bbox_inches='tight')
fig,ax = plt.subplots(1);ax.imshow(np.transpose(diffFSL[t,:,::-1]),vmin=-2,vmax=2);ax.axes.axis('off')
fig.savefig(folder_name+str(t)+'_fslDiff',bbox_inches='tight')