# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* EPI distortion(field)-maps estimation using a convolutional net (based on flownet)
* v 0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

git clone https://benjamin_zahneisen@bitbucket.org/benjamin_zahneisen/distestimation.git <repo_dir>
cd <repo_dir>
virtualenv -p python3.7 distNetEnv 
source ./distNetEnv/bin/activate 
pip install -r requirements.txt
jupyter notebook &
open example.ipynb


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* benjamin.zahneisen@gmail.com
