#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  8 16:56:36 2018

@author: bzahneisen
"""

import numpy as np
import cv2
import matplotlib.pyplot as plt
from matplotlib.pyplot import imshow
import keras.backend as K
from keras.utils import plot_model

#import the flow model
#from backend import memmap2ndarray
from utils import getDatasets,loadNiftiDatasets,_extractTrainingData,_resizeUpDownDatasets,vol2mosaic,loadNiftiVol
from distEstimatorNet.preprocessInput import preprocessInputData
from distEstimatorNet.distEstimator import UpDownFlow
from utils import displayResults

base_folder = '/home/bzahneisen/upDownDataSingleVol'
#base_folder = '/Users/bzahneisen/MRData/upDownData'
#base_folder = '/home/bzahneisen/upDownData'
datasetsInfo = getDatasets(base_folder)
upDownDatasets = loadNiftiDatasets(base_folder,[datasetsInfo[0]])
upDownDatasets_N128 = _resizeUpDownDatasets(upDownDatasets,(128,128))
trainingData = _extractTrainingData(upDownDatasets_N128)


##### data augmentation ###
#1. up/down flip
augm_trainingData1 = np.flip(trainingData,3)
#2. flip AP
augm_trainingData2 = np.flip(trainingData,2)
#3. shifts? 


#combined
trainingData = np.concatenate((trainingData,augm_trainingData1,augm_trainingData2),axis=0)

#load fsl map
fsl = loadNiftiVol(base_folder + '/0001/fsl128/fmap128.nii.gz') #[Hz]
#fsl = cv2.resize(fsl,(128,128))
Tacq = 0.076436 #acquisition time [s]
#gamma = 42.57*(10**6)
t_esp = Tacq/180 #full resolution echo spacing
# pixelShift = gamma*B0*N*t_esp ; t_esp = echo spacing
vdm0 = fsl *128*t_esp
vdm0 = np.transpose(vdm0,(2,0,1))

#load fsl-map for volume 00004
vdm2 = np.load('/home/bzahneisen/upDownDataSingleVol/0004/fsl/vdm.npy')


######prepare data
n_train_full,n_trainN64,n_trainN32,n_trainN16 = preprocessInputData(trainingData[:,:,:,:])
inputImgs = [n_train_full,n_trainN64,n_trainN32,n_trainN16]

###############################
#   Construct the model 
###############################
updownflow = UpDownFlow(architecture= 'SimpleFlowNet_v3',input_size = 128,batchNormFlag=True)
updownflow.config['w_tv'] = 0.2
updownflow.config['w_tvimg'] = 0.0001 #very low weighting for TV on corrected images

###############################
#   compile model 
###############################
updownflow.compileModel(learning_rate=0.001,w2=0.1,optimizer='adam')

###############################
#   Start the training process 
###############################
Nepochs = 50
hist=updownflow.train(inputImgs,[],Nepochs)

#predict 
prediction = updownflow.predict([n_train_full,n_trainN64,n_trainN32,n_trainN16])
out,out64,out32,out16,vdm,vdm64,vdm32,vdm16 = prediction

displayResults(prediction,inputImgs,5)

############################################
# DEBUG
############################################

############################################
# call the vdm layer separately for down
############################################
a=np.reshape(-vdm2,(72,128,128,1))
b=n_train_full[0:72,:,:,:]
#get vdm layer
f = updownflow.feature_extractor.model.get_layer('final_output')
VDMfun = K.function(f.input,[f.output])
layerOut = VDMfun([a,b])[0] #these two lines work
out = np.transpose(layerOut[:,:,:,0],(1,2,0))
out2 = np.transpose(layerOut[:,:,:,1],(1,2,0))
h= imshow(vol2mosaic(out),cmap='gray',vmax=5);plt.colorbar(h)

fig,ax = plt.subplots(1)
ax.imshow(uncorr[:,:,25],cmap='gray',vmax=5);plt.colorbar(h)
fig,ax = plt.subplots(1)
ax.imshow(corr[:,:,25],cmap='gray',vmax=5);plt.colorbar(h)
fig,ax = plt.subplots(1)
ax.imshow(wcorr[:,:,25],cmap='gray',vmax=5);plt.colorbar(h)
fig,ax = plt.subplots(1)
ax.imshow(wcorr_filt[:,:,25],cmap='gray',vmax=5);plt.colorbar(h)

fig,ax = plt.subplots(1)
ax.imshow(vdm2[25,:,:],cmap='gray');

fig,ax = plt.subplots(1)
ax.imshow(out2[:,:,25],cmap='gray',vmax=5);plt.colorbar(h)
     
############################################
# debug end of encoder layer
############################################
def plotLayer(m_model,inputData,name):
    conv_1=m_model.get_layer("conv_1")
    conv_out=m_model.get_layer(name)
    encoder = K.function([conv_1.input],[conv_out.output])
    encoderOut = encoder([n_upDownInput])[0] #these two lines work
    h=imshow(vol2mosaic(encoderOut[5,:,:,:16]),cmap='gray');plt.colorbar(h)


plotLayer(updownflow.feature_extractor.model,n_upDownInput,"flow2_postActivation");
plotLayer(updownflow.feature_extractor.model,n_upDownInput,"vdm_postActivation");
plotLayer(updownflow.feature_extractor.model,n_upDownInput,"final_output")


#call loss function 
loss_image=updownflow.custom_loss(encoderOut,encoderOut)
K.eval(loss_image)

#what's the ground truth loss
loss_true=updownflow.custom_loss(encoderOut,encoderOut)


#up down corrected
h=imshow(out[t,:,:,0]);plt.colorbar(h);
h=imshow(n_train_full[t,:,:,0])

#calculate sobel loss separately
tmp = K.variable(value = out[t:t+1,:,:,0:1])
tmp=updownflow.sobel.call(tmp)
sobDown = K.get_value(tmp)