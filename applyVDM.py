# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script in order to test different interpolation methods.
Strictly in numpy (NO tensorflow)

Options:
    1) scipy.ndimage.map_coordinates
    2) my 1d linear interpolation
    3) my 1d cubic interpolation
"""

import os,sys
import nibabel as nib
import numpy as np
import cv2
from scipy import ndimage

import matplotlib.pyplot as plt
from matplotlib.pyplot import imshow

def memmap2ndarray(x):

    dim = x.shape

    y = np.zeros(dim)
    for i in range(dim[0]):
        for j in range(dim[1]):
            for k in range(dim[2]):
                y[i,j,k] = float(x[i,j,k])
                
    return y


############################################
# unwarp using 1d linear interpolation (applied to one line)
############################################
def unwarp_1d_linear(vdm_line,line):
    
    Nx = np.shape(vdm_line)[0]
    
    #original grid f(u)
    u = np.linspace(0,Nx-1,Nx)
    
    #map to new coordinate x=x(u) with g(x)
    x = u + vdm_line
    
    x0 = np.linspace(0,Nx-1,Nx)
    #inverse map
    uprime = x0 - vdm_line
    
    ul = np.floor(uprime).astype(int)
    ur = ul + 1
    
    ul = np.clip(ul,0,Nx-1)
    ur = np.clip(ur,0,Nx-1)
    
    #index -1 wraps around
    gx = line[ul] + (line[ur]-line[ul] * (uprime - ul))
    
    return gx


############################################
# unwarp using 1d cubic interpolation (applied to one line)
############################################
def unwarp_1d_cubic(vdm_line,line):
    
    Nx = np.shape(vdm_line)[0]
    
    #grid of new image
    x0 = np.linspace(0,Nx-1,Nx)
    
    #inverse map
    uprime = x0 - vdm_line
    
    #get two point left and 2 points right of uprime
    ul = np.floor(uprime).astype(int)
    ul2 = ul-1
    ur = ul + 1
    ur2 = ul + 2
    
    ul = np.clip(ul,0,Nx-1)
    ul2 = np.clip(ul2,0,Nx-1)
    ur = np.clip(ur,0,Nx-1)
    ur2 = np.clip(ur2,0,Nx-1)
    
    #calculate weights
    w = np.zeros_like(vdm_line)
    for t in range(Nx):
        gi = ul[t]
        gi = np.clip(gi,0,Nx-1)      
        giplus1 = gi+1
        giplus1 = np.clip(giplus1,0,Nx-1)
        dwgi = (1 - (uprime[t]-ul[t]))
        dwgiplus1 = (uprime[t]-ul[t])
        w[gi] = w[gi] + dwgi
        w[giplus1] = w[giplus1] + dwgiplus1
    
    box = np.ones(3)/3
    w=np.convolve(w,box,mode='same')
    
    line_hat = np.divide(line,w)
    line = line_hat
    
    
    # f(p0,p1,p2,p3,x) = 
    cubicTerm = (-0.5*line[ul2] + 3/2*line[ul] -3/2*line[ur] +1/2*line[ur2])*np.power(uprime-ul,3)
    quadraticTerm = (line[ul2] - 5/2*line[ul] +2*line[ur] -1/2*line[ur2])*np.power(uprime-ul,2)
    linearTerm = (-1/2*line[ul2] + 1/2*line[ur])*(uprime-ul)
    zeroTerm = line[ul]
    
    gx = cubicTerm + quadraticTerm + linearTerm + zeroTerm
    
    return gx


############################################
# unwarp using 1d cubic interpolation (to be implemented in tensor notation)
############################################
def unwarp_Cubic(vdm,epi,polarity=1):
    #input : vdm = [m x Nx x Ny ]
    #           epi = [m x Nx x Ny x 1]
    
    batch_size=np.shape(vdm)[0] 
    width = np.shape(vdm)[1]
    height = np.shape(vdm)[2]
    
    Ny = height
    Nx = width
    
    #grid of new image
    x_linspace = np.linspace(0,width-1, width)
    y_linspace = np.linspace(0,height-1, height)
    
    y0 = np.reshape(y_linspace,(1,1,Ny))
    y0 = np.tile(y0,(batch_size,Nx,1)) #[m,Nx,Ny]
        
    #meshgrid as in matlab
    #x_coordinates, y_coordinates = np.meshgrid(x_linspace, y_linspace)
    #x_coordinates = np.reshape(x_coordinates, [-1]) #1d-array [Nx*Ny, 1]
    
    #ordering is different in matlab. Here we start with last dimension 
    #y_coordinates = np.reshape(y_coordinates, [-1])
    
    #y_coordinates_batch = np.tile(np.reshape(y_coordinates,(1,width,height)),(batch_size,1,1))
    #y_coordinates_batch = np.reshape(y_coordinates_batch,[-1])    
    
    #make image tensor array shaped
    vdm_flat = np.reshape(vdm,[-1])
    epi_flat = np.reshape(epi,[-1])
    y0 = np.reshape(y0,[-1])
    
    #inverse map
    uprime = y0 - vdm_flat
    
    #get two point left and 2 points right of uprime
    ul = np.floor(uprime).astype(int)
    ul2 = ul-1
    ur = ul + 1
    ur2 = ul + 2
    
    ul = np.clip(ul,0,Ny-1)
    ul2 = np.clip(ul2,0,Ny-1)
    ur = np.clip(ur,0,Ny-1)
    ur2 = np.clip(ur2,0,Ny-1)
    
    y = uprime-ul
    
    #generate x-offset
    flat_image_dimensions = width*height
    x_linspace = np.linspace(0,flat_image_dimensions-Ny, Ny)
    x0 = np.reshape(x_linspace,(1,Nx,1))
    x0 = np.tile(x0,(batch_size,1,Ny)) #[m,Nx,Ny]
    x0 = np.reshape(x0,[-1])
    x0 = x0.astype(int)
    
    
    flat_image_dimensions = width*height
    pixels_batch =  np.linspace(0,batch_size-1,batch_size)*flat_image_dimensions 
    pixels_batch = np.reshape(pixels_batch,(batch_size,1,1))
    pixels_batch = np.tile(pixels_batch,(1,Nx,Ny))    
    pixels_batch_flat = np.reshape(pixels_batch,[-1])
    #pixels_batch  = 0
    #flat_output_dimensions = output_height*output_width
    y_base = pixels_batch_flat.astype(int) #indexes into batches
    
    ul_indices = ul + y_base +x0
    ul2_indices = ul2 + y_base + x0
    ur_indices = ur + y_base +x0
    ur2_indices = ur2 + y_base +x0
    
    cubicTerm = (-0.5*epi_flat[ul2_indices] + 3/2*epi_flat[ul_indices] -3/2*epi_flat[ur_indices] +1/2*epi_flat[ur2_indices])*np.power(y,3)
    quadraticTerm = (epi_flat[ul2_indices] - 5/2*epi_flat[ul_indices] +2*epi_flat[ur_indices] -1/2*epi_flat[ur2_indices])*np.power(y,2)
    linearTerm = (-1/2*epi_flat[ul2_indices] + 1/2*epi_flat[ur_indices])*(y)
    zeroTerm = epi_flat[ul_indices]
    
    unwarp_epi = cubicTerm + quadraticTerm + linearTerm + zeroTerm
    
    unwarp_epi = np.reshape(unwarp_epi,(batch_size,width,height))
    
    return unwarp_epi


def _meshgrid(height, width):
        
    x_linspace = np.linspace(-1., 1., width)
    y_linspace = np.linspace(-1., 1., height)
        
    #meshgrid as in matlab
    x_coordinates, y_coordinates = np.meshgrid(x_linspace, y_linspace)
    x_coordinates = np.reshape(x_coordinates, [-1]) #1d-array
    y_coordinates = np.reshape(y_coordinates, [-1])
        
    #ones = np.ones_like(x_coordinates)
    #indices_grid = np.stack([x_coordinates, y_coordinates, ones], 0)
        
    #3 x (width x height), what's the one doing? 1 is for the 3x3 homogeneous transform
    return x_coordinates,y_coordinates


def _repeat(x, num_repeats):
    ones = np.ones((1, num_repeats))
    x = np.reshape(x, (-1,1))
    x = np.matmul(x, ones)
    return np.reshape(x, [-1])


############################################
# unwarp using bi-linear interpolation
############################################
def biLinearUnwarp(vdm,epi):
    
    #vdm = [batch Nx Ny]
    batch_size=1 #for now
    width = np.shape(vdm)[0]
    height = np.shape(vdm)[1]
    
    output_height = width
    output_width = height
    
    x_coordinates,y_coordinates = _meshgrid(output_height, output_width) #(width x height) x 3 in range [-1 1] 
    
    vdm_array = np.reshape(vdm,[-1])
    
    x = x_coordinates
    y = y_coordinates
    
    #generate x and y
    x = .5*(x + 1.0)*(width) #x comes in range [-1 1] transformded to [0 128]
    y = .5*(y + 1.0)*(height)
    
    yprime = y + vdm_array
    
    
    x0 = np.floor(x).astype(int) # round down = pixel value in image "left of x"
    x1 = x0 + 1 # "right of x" 
    y0 = np.floor(yprime).astype(int)
    y1 = y0 + 1
    
    max_y = height - 1 # true image dimensions
    max_x = width - 1
    
    x0 = np.clip(x0,0, max_x)
    x1 = np.clip(x1,0, max_x)
    y0 = np.clip(y0,0, max_y)
    y1 = np.clip(y1,0, max_y)
    
    flat_image_dimensions = width*height
    pixels_batch =  np.linspace(0,batch_size-1,batch_size)*flat_image_dimensions 
    #pixels_batch  = 0
    flat_output_dimensions = output_height*output_width
    base = _repeat(pixels_batch, flat_output_dimensions) #indexes into batches
    
    base_y0 = base + y0*width #if batch_size > 1, y0 must contain coordinates for all images in one long array
    base_y1 = base + y1*width
    indices_a = base_y0 + x0
    indices_b = base_y1 + x0
    indices_c = base_y0 + x1
    indices_d = base_y1 + x1
    
    flat_image = np.reshape(epi, (-1))
    #flat_image = tf.cast(flat_image, dtype='float32')
    #pixel_values_a = tf.gather(flat_image, indices_a) #get values from flat_image according to indices_a
    #pixel_values_b = tf.gather(flat_image, indices_b)
    #pixel_values_c = tf.gather(flat_image, indices_c)
    #pixel_values_d = tf.gather(flat_image, indices_d)
    pixel_values_a = flat_image[indices_a.astype(int)] #f(x,y) at the four corners
    pixel_values_b = flat_image[indices_b.astype(int)]
    pixel_values_c = flat_image[indices_c.astype(int)]
    pixel_values_d = flat_image[indices_d.astype(int)]

    area_a = ((x1 - x) * (y1 - y))
    area_b = ((x1 - x) * (y - y0))
    area_c = ((x - x0) * (y1 - y))
    area_d = ((x - x0) * (y - y0))
    
   # unwrp_epi = tf.add_n([area_a*pixel_values_a,
   #                        area_b*pixel_values_b,
   #                        area_c*pixel_values_c,
   #                    area_d*pixel_values_d])

    unwrp_epi = area_a*pixel_values_a + area_b*pixel_values_b + area_c*pixel_values_c + area_d*pixel_values_d
    
    unwrp_epi = np.reshape(unwrp_epi,(width,height))
    
    return unwrp_epi





############################################
# unwarp epi using scipy.ndimage.map_coordinates
############################################
def unwarpEPI(epi,vdm,polarity):
    
    dims = epi.shape
    
    x = np.array(range(dims[0]))
    y = np.array(range(dims[1])) 
    z = np.array(range(dims[2]))     
    #X, Y, Z = np.meshgrid(x, y,z)
    X, Y, Z = np.mgrid[0:dims[0],0:dims[1],0:dims[2]]
    
    #from scipy nd-image stuff
    #x = X[:,:,18]
    x = np.reshape(X,(np.prod(dims),1))
    #y = Y[:,:,18]
    y = np.reshape(Y,(np.prod(dims),1))
    z = np.reshape(Z,(np.prod(dims),1))
    yprime = y + polarity*np.reshape(vdm,(np.prod(dims),1))
    wrp_epi= ndimage.map_coordinates(epi, [x, yprime,z],order=3)
    
    wrp_epi = np.reshape(wrp_epi,dims)
    
    return wrp_epi

def unwarpEPI2d(epi,vdm,polarity):
    
    dims = epi.shape
    
    x = np.array(range(dims[0]))
    y = np.array(range(dims[1])) 

    X, Y = np.mgrid[0:dims[0],0:dims[1]]
    
    #from scipy nd-image stuff
    x = np.reshape(X,(np.prod(dims),1))
    y = np.reshape(Y,(np.prod(dims),1))
    yprime = y + polarity*np.reshape(vdm,(np.prod(dims),1))
    wrp_epi= ndimage.map_coordinates(epi, [x, yprime],order=1)
    
    wrp_epi = np.reshape(wrp_epi,dims)
    
    
    #get jacobi matrix
    jac = np.gradient(vdm,axis=1)
    
    wrp_epi = np.multiply(wrp_epi,(1 + polarity*jac))
    
    return wrp_epi
    
    
    
    return wrp_epi_up
    
#load niftis
epi_down = nib.load('/Users/bzahneisen/Dropbox/AI/fmapData/b0_down.nii')
epi_down_img = epi_down.get_data()
epi_down_img= memmap2ndarray(epi_down_img)

epi_up = nib.load('/Users/bzahneisen/Dropbox/AI/fmapData/b0_up.nii')
epi_up_img = epi_up.get_data()
epi_up_img = memmap2ndarray(epi_up_img)

vdm0 = nib.load('/Users/bzahneisen/Dropbox/AI/fmapData/vdm5_fpm_fieldmap.img')
vdm0_imag = vdm0.get_data()


epi_corr = nib.load('/Users/bzahneisen/Dropbox/AI/fmapData/my_hifi_b0.nii')
epi_corr = epi_corr.get_data()
epi_corr_img = np.zeros((180,180,36,2))
epi_corr_img[:,:,:,0] = memmap2ndarray(epi_corr[:,:,:,0])
epi_corr_img[:,:,:,1] = memmap2ndarray(epi_corr[:,:,:,1])

vdm_line = vdm0_imag[70,:,5]
epi_line = epi_down_img[70,:,5]

epi_unwarp = np.zeros((180,180))
epi_unwarp2 = np.zeros((180,180))
for t in range(180):
    epi_unwarp[t,:] =unwarp_1d_linear(vdm0_imag[t,:,5],epi_down_img[t,:,5]) 
    epi_unwarp2[t,:] =unwarp_1d_linear(-vdm0_imag[t,:,5],epi_up_img[t,:,5]) 

imshow(epi_unwarp,cmap='gray')
imshow(epi_unwarp2,cmap='gray')


epi_unwarp_cube = np.zeros((180,180))
epi_unwarp2_cube = np.zeros((180,180))
for t in range(180):
    epi_unwarp_cube[t,:] =unwarp_1d_cubic(vdm0_imag[t,:,5],epi_down_img[t,:,5]) 
    epi_unwarp2_cube[t,:] =unwarp_1d_cubic(-vdm0_imag[t,:,5],epi_up_img[t,:,5]) 

imshow(epi_unwarp_cube,cmap='gray')

imshow(epi_unwarp2_cube,cmap='gray')



#call on batches
unwrp_batch = unwarp_Cubic(np.transpose(vdm0_imag,(2, 0, 1)),np.transpose(epi_down_img,(2, 0, 1)),1)