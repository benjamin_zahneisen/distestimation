#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  8 16:56:36 2018

@author: bzahneisen
"""

import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import imshow

#import the flow model
from utils import getDatasets,loadNiftiDatasets,_extractTrainingData,_resizeUpDownDatasets
from utils import displayResults,vol2mosaic
from distEstimatorNet.preprocessInput import preprocessInputData
from distEstimatorNet.distEstimator import UpDownFlow


base_folder = '/home/bzahneisen/upDownData'
#base_folder = '/Users/bzahneisen/MRData/upDownData'
datasetsInfo = getDatasets(base_folder)
upDownDatasets = loadNiftiDatasets(base_folder,datasetsInfo,maxVolsPerDataset = 16,volInc=4)
upDownDatasets_N128 = _resizeUpDownDatasets(upDownDatasets,(128,128))
trainingData = _extractTrainingData(upDownDatasets_N128,normalize=True)

base_folder_test = '/home/bzahneisen/upDownDataTest'
upDownDatasetsTest = loadNiftiDatasets(base_folder_test,getDatasets(base_folder_test))
upDownDatasetsTest_N128 = _resizeUpDownDatasets(upDownDatasetsTest,(128,128))
testData = _extractTrainingData(upDownDatasetsTest_N128,normalize=True)

#shuffle subset of testData
#n=testData.shape[0]
#subIdx = np.random.choice(testData.shape[0],n,replace=False)
#testDataSub = testData[subIdx,:,:,:]

##### data augmentation ###
#1. up/down flip
augm_trainingData1 = np.flip(trainingData,3)
#2. flip AP
augm_trainingData2 = np.flip(trainingData,2)
#3. shifts? 
#probably not necessary

#combined
trainingData = np.concatenate((trainingData,augm_trainingData1,augm_trainingData2),axis=0)

#shuffle training data
n=int(trainingData.shape[0])
subIdx = np.random.choice(trainingData.shape[0],n,replace=False)
trainingData = trainingData[subIdx,:,:,:]

######prepare data (normalization is now done on a volume level)
n_train_full,n_trainN64,n_trainN32 = preprocessInputData(trainingData[:,:,:,:])
inputImgs = [n_train_full,n_trainN64,n_trainN32]
#test data
n_test_full,n_testN64,n_testN32 = preprocessInputData(testData[::1,:,:,:])
inputTest = [n_test_full,n_testN64,n_testN32]

###############################
#   Construct the model 
###############################
updownflow = UpDownFlow(architecture= 'SimpleFlowNet_v4',input_size = 128,batchNormFlag=True)
updownflow.config['w_tv'] = 0.05

###############################
#   compile model 
###############################
updownflow.compileModel(learning_rate=0.001,w2=0.1,optimizer='adam')

###############################
#   Start the training process with multiple runs
###############################
Nruns = 5
Nepochs = (20,20,20,20,20)
Lambda = (1,1,1,1,1) #labmda=0.08 seems to work well
lr = (0.001,0.001,0.001,0.001,0.001) #lr = 0.0001 was defualt lr May,1
log_folder = './v4_fullTraining_0611_lmba1_m4428'
if not os.path.exists(log_folder):
    os.mkdir(log_folder)

for n in range(Nruns):
    updownflow.config['w_tv'] = Lambda[n]
    updownflow.compileModel(learning_rate=lr[n],w2=0.1,optimizer='adam') 
    hist_phasen=updownflow.train(inputImgs,inputTest,Nepochs[n])
    updownflow.save('run_'+str(n)+'.h5',log_folder)
loss,valLoss = updownflow.plot_loss(1,0.183)
np.save(os.path.join(log_folder,'loss'),loss)
np.save(os.path.join(log_folder,'valLoss'),valLoss)
## end of training phases ##


#predict 
prediction = updownflow.predict(inputImgs)
out,out64,out32,vdm,vdm64,vdm32 = prediction

t=3000
displayResults(prediction,inputImgs,t)

############################################
# DEBUG
############################################
#plot_model(updownflow.feature_extractor.model, to_file='model.png',show_shapes=True)
for t in [5,20,40,80,100,130]: 
    displayResults(prediction,inputImgs,t)



############################################
# predict on 'unseen' test case
############################################
predictionTest = updownflow.predict([n_test_full,n_testN64,n_testN32])
outTest,out64Test,out32Test,vdmTest,vdm64Test,vdm32Test = predictionTest

for t in [5,30,200,300,400,420,500]:
    displayResults(predictionTest,inputTest,t)

    

