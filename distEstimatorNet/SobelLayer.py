#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  6 14:53:57 2018

@author: bzahneisen
"""

from keras import backend as K
from keras.layers.core import Layer
import tensorflow as tf
import numpy as np

class SobelOperator(object):
    def __init__(self,dx=0,dy=1):
        
        sobelKernely=np.zeros((3,3))
        sobelKernely[0][0] = 1;sobelKernely[1][0] = 2;sobelKernely[2][0] = 1
        sobelKernely[0][2] = -1;sobelKernely[1][2] = -2;sobelKernely[2][2] = -1
        sobelKernely = np.reshape(sobelKernely,[3,3,1,1]) # 1= input channels, 1= output channels
        self.kernely = K.variable(value=sobelKernely)
        
    def call(self,x):
        
        fx = K.conv2d(x,self.kernely,padding = 'same')
        
        fx = fx/8 #account for kernel
        
        return fx


class SobelLayer(Layer):
    
    def __init__(self, downsample_factor=1, **kwargs):
        super(SobelLayer, self).__init__(**kwargs)
        self.downsample_factor = downsample_factor
        
        
    def build(self, input_shape):
        
        
        self.built = True
        
        
        
        
    def compute_output_shape(self, input_shape):
        output_size = self.output_size
        return (None,
                int(output_size[0]),
                int(output_size[1]),
                int(input_shape[-1]))
        
        
    def call(self, x, mask=None):
        # theta should be shape (batchsize, 2, 3)
        # see eq. (1) and sec 3.1 in ref [2]
        conv_input, theta = x
        
        output = K.dot(x)
        
        #output = _transform(theta, conv_input, self.downsample_factor)
        return output