#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar  3 16:34:11 2018

@author: bzahneisen
"""
import numpy as np
#import cv2
import scipy.ndimage as sc

"""these functions are part of the "Net" because they contain hyperparameters like 
how much smoothing for low resolution input maps,..."""

def normalizeInputPair(X,zeroMeanFlag=False):
    #X = pair of up/down input images
    snr = 100
    m1 = np.greater(X[:,:,0],1/snr*np.max(X))
    mean_tissue = np.mean(X[m1])
    if zeroMeanFlag == True:
        mean = np.mean(X)
        nX = np.divide(X-mean,mean)
        #print('zero mean')
    else:
        nX = np.divide(X,mean_tissue) 
    
    return nX,mean_tissue

# normalize volumes (=less weighting for low signal slices)
def normalizeInputVol(X):
    #X = pair of up/down input volumes
    snr = 100
    m1 = np.greater(X[:,:,:,0],1/snr*np.max(X))
    mean_tissue_signal = np.mean(X[m1])
    
    nX = np.divide(X,mean_tissue_signal)
    
    return nX,mean_tissue_signal


def preprocessInputData(trainingData):
    #1.normalize input images separately
    #2.filter + resize -> 64x64 images [5x5 kernel, sigma = 2]
    #3.filter + resize -> 32x32 images [5x5 kernel, sigma = 4]

    n_trainingData_full = np.zeros(trainingData.shape)
    mean_tissue_slices = np.zeros((n_trainingData_full.shape[0],1))
    #for m in range(n_trainingData_full.shape[0]):
    #    n_trainingData_full[m,:,:,:],mean_tissue_slices[m] = normalizeInputPair(trainingData[m,:,:,:])
    #no normalization here
    n_trainingData_full = trainingData

    #continue with normalized data
    n_trainingData_N64 = np.zeros((n_trainingData_full.shape[0],64,64,2))
    for m in range(n_trainingData_full.shape[0]):
        #f0 = cv2.GaussianBlur(n_trainingData_full[m,:,:,0],(5,5),1.5)
        #f1 = cv2.GaussianBlur(n_trainingData_full[m,:,:,1],(5,5),1.5)
        #n_trainingData_N64[m,:,:,0] = cv2.resize(f0,(64,64))
        #n_trainingData_N64[m,:,:,1] = cv2.resize(f1,(64,64))
        f0 = sc.gaussian_filter(n_trainingData_full[m,:,:,0],1.2)
        f1 = sc.gaussian_filter(n_trainingData_full[m,:,:,1],1.2)
        n_trainingData_N64[m,:,:,0] = sc.zoom(f0,0.5)
        n_trainingData_N64[m,:,:,1] = sc.zoom(f1,0.5)

    n_trainingData_N32 = np.zeros((n_trainingData_full.shape[0],32,32,2))
    for m in range(n_trainingData_full.shape[0]):
        #f0 = cv2.GaussianBlur(n_trainingData_full[m,:,:,0],(7,7),4)
        #f1 = cv2.GaussianBlur(n_trainingData_full[m,:,:,1],(7,7),4)
        #n_trainingData_N32[m,:,:,0] = cv2.resize(f0,(32,32))
        #n_trainingData_N32[m,:,:,1] = cv2.resize(f1,(32,32))
        f0 = sc.gaussian_filter(n_trainingData_full[m,:,:,0],2)
        f1 = sc.gaussian_filter(n_trainingData_full[m,:,:,1],2)
        n_trainingData_N32[m,:,:,0] = sc.zoom(f0,0.25)
        n_trainingData_N32[m,:,:,1] = sc.zoom(f1,0.25)
        
        
    return n_trainingData_full,n_trainingData_N64,n_trainingData_N32
