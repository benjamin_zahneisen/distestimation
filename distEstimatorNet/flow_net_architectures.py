#!/usr/bin/env python3


from keras.models import Model
import tensorflow as tf
from keras.layers import Reshape, Activation, Conv2D, Input, MaxPooling2D, BatchNormalization, Flatten, Dense, Lambda, UpSampling2D
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.merge import concatenate
import keras.backend as K
from distEstimatorNet.backend import applyVDM,applyVDM_wrapper,applyVDM_compact
import numpy as np
#import sys
#sys.path.append('/Users/bzahneisen/Dropbox/AI/distortionEstimation/3p_code')
from distEstimatorNet.spatial_transformer import SpatialTransformer as asd


def gaussian(x, mu, sigma):
    return np.exp(-(float(x) - float(mu)) ** 2 / (2 * sigma ** 2))


def make_kernel(sigma):
    # kernel radius = 2*sigma, but minimum 3x3 matrix
    kernel_size = max(3, int(2 * 2 * sigma + 1))
    mean = np.floor(0.5 * kernel_size)
    kernel_1d = np.array([gaussian(x, mean, sigma) for x in range(kernel_size)])
    # make 2D kernel
    np_kernel = np.outer(kernel_1d, kernel_1d).astype(dtype=K.floatx())
    # normalize kernel by sum of elements
    kernel = np_kernel / np.sum(np_kernel)
    return kernel

#-----------------------------------------------#
# base class for model
#-----------------------------------------------#
class BaseFeatureExtractor(object):
    """docstring for ClassName"""

    # to be defined in each subclass
    def __init__(self, input_size):
        raise NotImplementedError("error message")

    # to be defined in each subclass
    def normalize(self, image):
        raise NotImplementedError("error message")       

    def get_output_shape(self):
        return self.model.get_output_shape_at(-1)[1:3]

    def extract(self, input_image):
        return self.feature_extractor(input_image)
#------------------end of base class--------------#


#-----------------------------------------------#
# not so simple flow net architecture with information flow from contracting layers 
# difference to _v3: conv1_1 layer (64x64), NO 16x16 flow map, 2nd layer for vdm map estimation 
# -enable bias switch
# 2018 April,12
#-----------------------------------------------#
class SimpleFlowNet_v4(BaseFeatureExtractor):
    """docstring for ClassName"""
    def __init__(self, input_size,useBatchNorm):
        
        useBias = True
        useDensityW = True
        
        #Glorot normal initializer, also called Xavier normal initializer
        myKernel_init = 'glorot_normal'
#        myKernel_init='glorot_uniform' #default for conv2d

        input_img = Input(shape=(input_size, input_size, 2),name='main_input')
        input_img64 = Input(shape=(input_size/2, input_size/2, 2),name = 'input64')
        input_img32 = Input(shape=(input_size/4, input_size/4, 2),name='input32')
        
                # Layer 0 (e.g. 128x128, c=16) 
        conv0 = Conv2D(16, (7,7), strides=(1,1), padding='same', name='conv0',kernel_initializer = myKernel_init, use_bias=useBias)(input_img)
        if useBatchNorm == True:
            conv0 = BatchNormalization(scale=False)(conv0)
        conv0 = LeakyReLU(alpha=0.1,name='layer0')(conv0)
        
        # Layer 0_1 (e.g. 128x128, c=16) (feed to layer before final prediction)
        conv0_1 = Conv2D(16, (5,5), strides=(1,1), padding='same', name='conv0_1',kernel_initializer = myKernel_init, use_bias=useBias)(conv0)
        if useBatchNorm == True:
            conv0_1 = BatchNormalization(scale=False)(conv0_1)
        conv0_1 = LeakyReLU(alpha=0.1,name='layer0_1')(conv0_1)

        # Layer 1 (e.g. 64x64, c=32)
        conv1 = Conv2D(32, (5,5), strides=(2,2), padding='same', name='conv1',kernel_initializer = myKernel_init, use_bias=useBias)(conv0)
        if useBatchNorm == True:
            conv1 = BatchNormalization(scale=False)(conv1)        
        conv1 = LeakyReLU(alpha=0.1,name='layer1')(conv1)

        # Layer 1_1 (e.g. 64x64, c=32)
        conv1_1 = Conv2D(32, (5,5), strides=(1,1), padding='same', name='conv1_1',kernel_initializer = myKernel_init, use_bias=useBias)(conv1)
        if useBatchNorm == True:
            conv1_1 = BatchNormalization(scale=False)(conv1_1)        
        conv1_1 = LeakyReLU(alpha=0.1,name='layer1_1')(conv1_1)

        #Layer 2 (e.g. 32x32, c = 64)
        conv2 = Conv2D(64, (3,3), strides=(2,2), padding='same', name='conv2',kernel_initializer = myKernel_init, use_bias=useBias)(conv1_1)
        if useBatchNorm == True:
            conv2 = BatchNormalization(scale=False)(conv2) 
        conv2 = LeakyReLU(alpha=0.1,name='layer2')(conv2)

        #Layer 2_1 (e.g. 32x32, c = 64)
        conv2_1 = Conv2D(64, (3,3), strides=(1,1), padding='same', name='conv2_1',kernel_initializer = myKernel_init, use_bias=useBias)(conv2)
        if useBatchNorm == True:
            conv2_1 = BatchNormalization(scale=False)(conv2_1) 
        conv2_1 = LeakyReLU(alpha=0.1,name='layer2_1')(conv2_1)

        #Layer 3 (e.g. 16x16, c = 128)
        conv3 = Conv2D(128, (3,3), strides=(2,2), padding='same', name='conv3',kernel_initializer = myKernel_init, use_bias=useBias)(conv2_1)
        if useBatchNorm == True:
            conv3 = BatchNormalization(scale=False)(conv3) 
        conv3 = LeakyReLU(alpha=0.1,name='layer3')(conv3)

        #Layer 3_1 (e.g. 16x16, c = 128)
        conv3_1 = Conv2D(128, (3,3), strides=(1,1), padding='same', name='conv3_1',kernel_initializer = myKernel_init, use_bias=useBias)(conv3)
        if useBatchNorm == True:
            conv3_1 = BatchNormalization(scale=False)(conv3_1)
        conv3_1 = LeakyReLU(alpha=0.1,name='layer3_1')(conv3_1) 
        
        #Layer 4 (e.g. 8x8, c = 256)
        conv4 = Conv2D(256, (3,3), strides=(2,2), padding='same', name='conv4',kernel_initializer = myKernel_init, use_bias=useBias)(conv3_1)
        if useBatchNorm == True:
            conv4 = BatchNormalization(scale=False)(conv4)
        conv4 = LeakyReLU(alpha=0.1,name='layer4')(conv4)
        #------ End of ENCODER -------#

        #now start the upsampling part (back to 16x16) --- upconvolution = unpooling + convolution + activation
        #c/2 for first up-conv
        upconv3 = UpSampling2D(size=(2, 2))(conv4)
        upconv3 = Conv2D(128, (3,3), strides=(1,1), padding='same', name='upconv3',kernel_initializer = myKernel_init, use_bias=useBias)(upconv3)
        if useBatchNorm == True:
            upconv3 = BatchNormalization(scale=False)(upconv3)
        upconv3 = LeakyReLU(alpha=0.1)(upconv3)

        #add information from contracting part --> (16x16x256)
        jointLayer3 = Lambda(lambda x : K.concatenate([x[0],x[1]],axis=-1),output_shape=(16,16,256),name='concat3')([upconv3,conv3_1])

        #upsampling of joint layer (--> 32x32x256)
        upconv2 = UpSampling2D(size=(2, 2))(jointLayer3) 
        #c/4 for subsequent up-convs (--> 32x32x128)
        upconv2 = Conv2D(64, (5,5), strides=(1,1), padding='same', name='upconv2',kernel_initializer = myKernel_init, use_bias=useBias)(upconv2)
        if useBatchNorm == True:
            upconv2 = BatchNormalization(scale=False)(upconv2)
        upconv2 = LeakyReLU(alpha=0.1)(upconv2)
        
        #combine upconv2 + conv2_1  (--> 32x32x(64)
        jointLayer2 =  Lambda(lambda x : K.concatenate([x[0],x[1]],axis=-1),output_shape=(32,32,64+64),name='2LayerConcat1')([upconv2,conv2_1])
        
        ## -- fork low resolution vdm (32x32x1) -- ##
        vdm32 = Conv2D(1, (3,3), strides=(1,1), padding='same', name='vdm32',kernel_initializer = myKernel_init, use_bias=useBias)(jointLayer2)
        #apply to 32x32 image
        out32 = Lambda(applyVDM_wrapper,name='out32',output_shape=(32,32,2), trainable=False,arguments={'densityWeighting':useDensityW,'sigma':0.8})([vdm32,input_img32])
        #up-convolve low resolution map
        vdm32up = UpSampling2D(size=(2, 2))(vdm32) # (64x64) that's the 'upconvolution bent arrow in paper
        vdm32up = Conv2D(1, (5,5), strides=(1,1), padding='same', name='vdm32_upconv',kernel_initializer = myKernel_init, use_bias=useBias)(vdm32up)
        ## -- vdm32up = (64x64x1) -- ##
        
        ##up-conv joint layer with c/4 (-->64x64x32)
        upconv1 = UpSampling2D(size=(2, 2))(jointLayer2) 
        upconv1 = Conv2D(32, (5,5), strides=(1,1), padding='same', name='upconv1',kernel_initializer = myKernel_init, use_bias=useBias)(upconv1)
        if useBatchNorm == True:
            upconv1 = BatchNormalization(scale=False)(upconv1)
        upconv1 = LeakyReLU(alpha=0.1)(upconv1)
        
        #combine upconv_1 + conv_1 +flow_2up (--> 64x64x(64+1)
        jointLayer1 =  Lambda(lambda x : K.concatenate([x[0],x[1],x[2]],axis=-1),output_shape=(64,64,64+1),name='3LayerConcat2')([upconv1,conv1_1,vdm32up])
        
        ## -- fork low resolution vdm (64x64x1) -- ##
        vdm64 = Conv2D(1, (5,5), strides=(1,1), padding='same', name='vdm64', use_bias=useBias)(jointLayer1)
        out64 = Lambda(applyVDM_wrapper,name='out64',output_shape=(input_size/2,input_size/2,2), trainable=False,arguments={'densityWeighting':useDensityW})([vdm64,input_img64])
        vdm64up = UpSampling2D(size=(2, 2))(vdm64) # (128x 128)
        vdm64up = Conv2D(1, (5,5), strides=(1,1), padding='same', name='vdm64_upconv',kernel_initializer = myKernel_init, use_bias=useBias)(vdm64up)
        ## - vdm64up = (128x128x1) --- ##
        
        #upconv jointLayer1 --> 128x128x32
        upconv0=UpSampling2D(size=(2, 2))(jointLayer1)   
        upconv0 = Conv2D(32, (5,5), strides=(1,1), padding='same', name='upconv0',kernel_initializer = myKernel_init, use_bias=useBias)(upconv0)
        if useBatchNorm == True:
            upconv0 = BatchNormalization(scale=False)(upconv0)
        upconv0 = LeakyReLU(alpha=0.1)(upconv0)
        
        lastLayer =  Lambda(lambda x : K.concatenate([x[0],x[1],x[2]],axis=-1),output_shape=(128,128,32+16+1),name='threeLayerConcat_final')([upconv0,conv0_1,vdm64up])        
        
        lastLayer = Conv2D(32, (5,5), strides=(1,1), padding='same', name='lastLayer',kernel_initializer = myKernel_init, use_bias=useBias)(lastLayer)
        lastLayer = LeakyReLU(alpha=0.1)(lastLayer)
        
        finalVDM = Conv2D(1, (5,5), strides=(1,1), padding='same', name='finalVDM',kernel_initializer = myKernel_init, use_bias=useBias)(lastLayer)
        
        #final distortion correction        
        out = Lambda(applyVDM_wrapper,name='final_output',output_shape=(input_size,input_size,2), trainable=False,arguments={'densityWeighting':useDensityW})([finalVDM,input_img])
        
        #create model
        self.model = Model(inputs=[input_img,input_img64,input_img32],outputs=[out,out64,out32,finalVDM,vdm64,vdm32])
        
        print("done generating flownet v4")
        
    def normalize(self, image):
        return image / np.argmax(image)

#-----------------------------------------------#
# not so simple flow net architecture with information flow from contracting layers 
# difference to _v3: conv1_1 layer (64x64), NO 16x16 flow map, 2nd layer for vdm map estimation 
# -enable bias switch
# 2018 April,12
#-----------------------------------------------#
class SimpleFlowNet_v4_smaller(BaseFeatureExtractor):
    """docstring for ClassName"""
    def __init__(self, input_size,useBatchNorm):
        
        useBias = True
        useDensityW = True
        
        #Glorot normal initializer, also called Xavier normal initializer
        myKernel_init = 'glorot_normal'
#        myKernel_init='glorot_uniform' #default for conv2d

        input_img = Input(shape=(input_size, input_size, 2),name='main_input')
        input_img64 = Input(shape=(input_size/2, input_size/2, 2),name = 'input64')
        input_img32 = Input(shape=(input_size/4, input_size/4, 2),name='input32')
        
                # Layer 0 (e.g. 128x128, c=16) 
        conv0 = Conv2D(8, (5,5), strides=(1,1), padding='same', name='conv0',kernel_initializer = myKernel_init, use_bias=useBias)(input_img)
        if useBatchNorm == True:
            conv0 = BatchNormalization()(conv0)
        conv0 = LeakyReLU(alpha=0.1,name='layer0')(conv0)
        
        # Layer 0_1 (e.g. 128x128, c=16) (feed to layer before final prediction)
        conv0_1 = Conv2D(8, (5,5), strides=(1,1), padding='same', name='conv0_1',kernel_initializer = myKernel_init, use_bias=useBias)(conv0)
        if useBatchNorm == True:
            conv0_1 = BatchNormalization()(conv0_1)
        conv0_1 = LeakyReLU(alpha=0.1,name='layer0_1')(conv0_1)

        # Layer 1 (e.g. 64x64, c=32)
        conv1 = Conv2D(16, (5,5), strides=(2,2), padding='same', name='conv1',kernel_initializer = myKernel_init, use_bias=useBias)(conv0)
        if useBatchNorm == True:
            conv1 = BatchNormalization()(conv1)        
        conv1 = LeakyReLU(alpha=0.1,name='layer1')(conv1)

        # Layer 1_1 (e.g. 64x64, c=32)
        conv1_1 = Conv2D(16, (5,5), strides=(1,1), padding='same', name='conv1_1',kernel_initializer = myKernel_init, use_bias=useBias)(conv1)
        if useBatchNorm == True:
            conv1_1 = BatchNormalization()(conv1_1)        
        conv1_1 = LeakyReLU(alpha=0.1,name='layer1_1')(conv1_1)

        #Layer 2 (e.g. 32x32, c = 64)
        conv2 = Conv2D(32, (3,3), strides=(2,2), padding='same', name='conv2',kernel_initializer = myKernel_init, use_bias=useBias)(conv1_1)
        if useBatchNorm == True:
            conv2 = BatchNormalization()(conv2) 
        conv2 = LeakyReLU(alpha=0.1,name='layer2')(conv2)

        #Layer 2_1 (e.g. 32x32, c = 64)
        conv2_1 = Conv2D(32, (3,3), strides=(1,1), padding='same', name='conv2_1',kernel_initializer = myKernel_init, use_bias=useBias)(conv2)
        if useBatchNorm == True:
            conv2_1 = BatchNormalization()(conv2_1) 
        conv2_1 = LeakyReLU(alpha=0.1,name='layer2_1')(conv2_1)

        #Layer 3 (e.g. 16x16, c = 128)
        conv3 = Conv2D(64, (3,3), strides=(2,2), padding='same', name='conv3',kernel_initializer = myKernel_init, use_bias=useBias)(conv2_1)
        if useBatchNorm == True:
            conv3 = BatchNormalization()(conv3) 
        conv3 = LeakyReLU(alpha=0.1,name='layer3')(conv3)

        #Layer 3_1 (e.g. 16x16, c = 128)
        conv3_1 = Conv2D(64, (3,3), strides=(1,1), padding='same', name='conv3_1',kernel_initializer = myKernel_init, use_bias=useBias)(conv3)
        if useBatchNorm == True:
            conv3_1 = BatchNormalization()(conv3_1)
        conv3_1 = LeakyReLU(alpha=0.1,name='layer3_1')(conv3_1) 
        
        #Layer 4 (e.g. 8x8, c = 256)
        conv4 = Conv2D(128, (3,3), strides=(2,2), padding='same', name='conv4',kernel_initializer = myKernel_init, use_bias=useBias)(conv3_1)
        if useBatchNorm == True:
            conv4 = BatchNormalization()(conv4)
        conv4 = LeakyReLU(alpha=0.1,name='layer4')(conv4)
        #------ End of ENCODER -------#

        #now start the upsampling part (back to 16x16) --- upconvolution = unpooling + convolution + activation
        #c/2 for first up-conv
        upconv3 = UpSampling2D(size=(2, 2))(conv4)
        upconv3 = Conv2D(64, (3,3), strides=(1,1), padding='same', name='upconv3',kernel_initializer = myKernel_init, use_bias=useBias)(upconv3)
        if useBatchNorm == True:
            upconv3 = BatchNormalization()(upconv3)
        upconv3 = LeakyReLU(alpha=0.1)(upconv3)

        #add information from contracting part --> (16x16x256)
        jointLayer3 = Lambda(lambda x : K.concatenate([x[0],x[1]],axis=-1),output_shape=(16,16,128),name='concat3')([upconv3,conv3_1])

        #upsampling of joint layer (--> 32x32x256)
        upconv2 = UpSampling2D(size=(2, 2))(jointLayer3) 
        #c/4 for subsequent up-convs (--> 32x32x128)
        upconv2 = Conv2D(32, (5,5), strides=(1,1), padding='same', name='upconv2',kernel_initializer = myKernel_init, use_bias=useBias)(upconv2)
        if useBatchNorm == True:
            upconv2 = BatchNormalization()(upconv2)
        upconv2 = LeakyReLU(alpha=0.1)(upconv2)
        
        #combine upconv2 + conv2_1  (--> 32x32x(64)
        jointLayer2 =  Lambda(lambda x : K.concatenate([x[0],x[1]],axis=-1),output_shape=(32,32,32+32),name='2LayerConcat1')([upconv2,conv2_1])
        
        ## -- fork low resolution vdm (32x32x1) -- ##
        vdm32 = Conv2D(1, (3,3), strides=(1,1), padding='same', name='vdm32',kernel_initializer = myKernel_init, use_bias=useBias)(jointLayer2)
        #apply to 32x32 image
        out32 = Lambda(applyVDM_wrapper,name='out32',output_shape=(32,32,2), trainable=False,arguments={'densityWeighting':useDensityW,'sigma':0.8})([vdm32,input_img32])
        #up-convolve low resolution map
        vdm32up = UpSampling2D(size=(2, 2))(vdm32) # (64x64) that's the 'upconvolution bent arrow in paper
        vdm32up = Conv2D(1, (5,5), strides=(1,1), padding='same', name='vdm32_upconv',kernel_initializer = myKernel_init, use_bias=useBias)(vdm32up)
        ## -- vdm32up = (64x64x1) -- ##
        
        ##up-conv joint layer with c/4 (-->64x64x32)
        upconv1 = UpSampling2D(size=(2, 2))(jointLayer2) 
        upconv1 = Conv2D(16, (5,5), strides=(1,1), padding='same', name='upconv1',kernel_initializer = myKernel_init, use_bias=useBias)(upconv1)
        if useBatchNorm == True:
            upconv1 = BatchNormalization()(upconv1)
        upconv1 = LeakyReLU(alpha=0.1)(upconv1)
        
        #combine upconv_1 + conv_1 +flow_2up (--> 64x64x(64+1)
        jointLayer1 =  Lambda(lambda x : K.concatenate([x[0],x[1],x[2]],axis=-1),output_shape=(64,64,32+1),name='3LayerConcat2')([upconv1,conv1_1,vdm32up])
        
        ## -- fork low resolution vdm (64x64x1) -- ##
        vdm64 = Conv2D(1, (5,5), strides=(1,1), padding='same', name='vdm64', use_bias=useBias)(jointLayer1)
        out64 = Lambda(applyVDM_wrapper,name='out64',output_shape=(input_size/2,input_size/2,2), trainable=False,arguments={'densityWeighting':useDensityW})([vdm64,input_img64])
        vdm64up = UpSampling2D(size=(2, 2))(vdm64) # (128x 128)
        vdm64up = Conv2D(1, (5,5), strides=(1,1), padding='same', name='vdm64_upconv',kernel_initializer = myKernel_init, use_bias=useBias)(vdm64up)
        ## - vdm64up = (128x128x1) --- ##
        
        #upconv jointLayer1 --> 128x128x32
        upconv0=UpSampling2D(size=(2, 2))(jointLayer1)   
        upconv0 = Conv2D(16, (5,5), strides=(1,1), padding='same', name='upconv0',kernel_initializer = myKernel_init, use_bias=useBias)(upconv0)
        if useBatchNorm == True:
            upconv0 = BatchNormalization()(upconv0)
        upconv0 = LeakyReLU(alpha=0.1)(upconv0)
        
        lastLayer =  Lambda(lambda x : K.concatenate([x[0],x[1],x[2]],axis=-1),output_shape=(128,128,16+8+1),name='threeLayerConcat_final')([upconv0,conv0_1,vdm64up])        
        
        lastLayer = Conv2D(16, (5,5), strides=(1,1), padding='same', name='lastLayer',kernel_initializer = myKernel_init, use_bias=useBias)(lastLayer)
        lastLayer = LeakyReLU(alpha=0.1)(lastLayer)
        
        finalVDM = Conv2D(1, (5,5), strides=(1,1), padding='same', name='finalVDM',kernel_initializer = myKernel_init, use_bias=useBias)(lastLayer)
        
        #final distortion correction        
        out = Lambda(applyVDM_wrapper,name='final_output',output_shape=(input_size,input_size,2), trainable=False,arguments={'densityWeighting':useDensityW})([finalVDM,input_img])
        
        #create model
        self.model = Model(inputs=[input_img,input_img64,input_img32],outputs=[out,out64,out32,finalVDM,vdm64,vdm32])
        
        print("done generating flownet v4 smaller")
        
    def normalize(self, image):
        return image / np.argmax(image)        

#-----------------------------------------------#
# not so simple flow net architecture with information flow from contracting layers 
# difference to _v2: conv1_1 layer (64x64), 16x16 flow map, option for batch normalization 
#-----------------------------------------------#
class SimpleFlowNet_v3(BaseFeatureExtractor):
    """docstring for ClassName"""
    def __init__(self, input_size,useBatchNorm):
        
        #Glorot normal initializer, also called Xavier normal initializer
        myKernel_init = 'glorot_normal'
#        myKernel_init='glorot_uniform' #default for conv2d

        input_img = Input(shape=(input_size, input_size, 2),name='main_input')
        input_img64 = Input(shape=(input_size/2, input_size/2, 2),name = 'lowRes_input64')
        input_img32 = Input(shape=(input_size/4, input_size/4, 2),name='lowRes_input32')
        input_img16 = Input(shape=(input_size/8, input_size/8, 2),name='lowRes_input16')        

        # Layer 0 (e.g. 128x128, c=16) 
        conv0 = Conv2D(16, (7,7), strides=(1,1), padding='same', name='conv0',kernel_initializer = myKernel_init, use_bias=False)(input_img)
        if useBatchNorm == True:
            conv0 = BatchNormalization(scale=False)(conv0)
        conv0 = LeakyReLU(alpha=0.1,name='layer0')(conv0)
        
        # Layer 0_1 (e.g. 128x128, c=16) (feed to layer before final prediction)
        conv0_1 = Conv2D(16, (5,5), strides=(1,1), padding='same', name='conv0_1',kernel_initializer = myKernel_init, use_bias=False)(conv0)
        if useBatchNorm == True:
            conv0_1 = BatchNormalization(scale=False)(conv0_1)
        conv0_1 = LeakyReLU(alpha=0.1,name='layer0_1')(conv0_1)

        # Layer 1 (e.g. 64x64, c=32)
        conv1 = Conv2D(32, (5,5), strides=(2,2), padding='same', name='conv1',kernel_initializer = myKernel_init, use_bias=False)(conv0)
        if useBatchNorm == True:
            conv1 = BatchNormalization(scale=False)(conv1)        
        conv1 = LeakyReLU(alpha=0.1,name='layer1')(conv1)

        # Layer 1_1 (e.g. 64x64, c=32)
        conv1_1 = Conv2D(32, (5,5), strides=(1,1), padding='same', name='conv1_1',kernel_initializer = myKernel_init, use_bias=False)(conv1)
        if useBatchNorm == True:
            conv1_1 = BatchNormalization(scale=False)(conv1_1)        
        conv1_1 = LeakyReLU(alpha=0.1,name='layer1_1')(conv1_1)

        #Layer 2 (e.g. 32x32, c = 64)
        conv2 = Conv2D(64, (3,3), strides=(2,2), padding='same', name='conv2',kernel_initializer = myKernel_init, use_bias=False)(conv1_1)
        if useBatchNorm == True:
            conv2 = BatchNormalization(scale=False)(conv2) 
        conv2 = LeakyReLU(alpha=0.1,name='layer2')(conv2)

        #Layer 2_1 (e.g. 32x32, c = 64)
        conv2_1 = Conv2D(64, (3,3), strides=(1,1), padding='same', name='conv2_1',kernel_initializer = myKernel_init, use_bias=False)(conv2)
        if useBatchNorm == True:
            conv2_1 = BatchNormalization(scale=False)(conv2_1) 
        conv2_1 = LeakyReLU(alpha=0.1,name='layer2_1')(conv2_1)

        #Layer 3 (e.g. 16x16, c = 128)
        conv3 = Conv2D(128, (3,3), strides=(2,2), padding='same', name='conv3',kernel_initializer = myKernel_init, use_bias=False)(conv2_1)
        if useBatchNorm == True:
            conv3 = BatchNormalization(scale=False)(conv3) 
        conv3 = LeakyReLU(alpha=0.1,name='layer3')(conv3)

        #Layer 3_1 (e.g. 16x16, c = 128)
        conv3_1 = Conv2D(128, (3,3), strides=(1,1), padding='same', name='conv3_1',kernel_initializer = myKernel_init, use_bias=False)(conv3)
        if useBatchNorm == True:
            conv3_1 = BatchNormalization(scale=False)(conv3_1)
        conv3_1 = LeakyReLU(alpha=0.1,name='layer3_1')(conv3_1) 
        
        #Layer 4 (e.g. 8x8, c = 256)
        conv4 = Conv2D(256, (3,3), strides=(2,2), padding='same', name='conv4',kernel_initializer = myKernel_init, use_bias=False)(conv3_1)
        if useBatchNorm == True:
            conv4 = BatchNormalization(scale=False)(conv4)
        conv4 = LeakyReLU(alpha=0.1,name='layer4')(conv4)
        #------ End of ENCODER -------#
        
        
        #now start the upsampling part (back to 16x16) --- upconvolution = unpooling + convolution + activation
        #c/2 for first up-conv
        upconv3 = UpSampling2D(size=(2, 2))(conv4)
        upconv3 = Conv2D(128, (3,3), strides=(1,1), padding='same', name='upconv3',kernel_initializer = myKernel_init, use_bias=False)(upconv3)
        if useBatchNorm == True:
            upconv3 = BatchNormalization(scale=False)(upconv3)
        upconv3 = LeakyReLU(alpha=0.1)(upconv3)
        
        #add information from contracting part --> (16x16x256)
        jointLayer3 = Lambda(lambda x : K.concatenate([x[0],x[1]],axis=-1),output_shape=(16,16,256),name='concat3')([upconv3,conv3_1])
        
        ## -- fork low resolution vdm (16x16x1) -- ##
        vdm16 = Conv2D(1, (3,3), strides=(1,1), padding='same', name='vdm16',kernel_initializer = myKernel_init, use_bias=False)(jointLayer3)
        #apply to 16x16 image
        out16 = Lambda(applyVDM_wrapper,name='out16',output_shape=(16,16,2), trainable=False,arguments={'densityWeighting':False,'sigma':0.0})([vdm16,input_img16])
        #up-convolve low resolution map
        vdm16up = UpSampling2D(size=(2, 2))(vdm16) # (32x32) that's the 'upconvolution bent arrow in paper
        vdm16up = Conv2D(1, (3,3), strides=(1,1), padding='same', name='vdm16_upconv',kernel_initializer = myKernel_init, use_bias=False)(vdm16up)
        ## -- vdm16up = (32x32x1) -- ##
        
        #upsampling of joint layer (--> 32x32x256)
        upconv2 = UpSampling2D(size=(2, 2))(jointLayer3) 
        #c/4 for subsequent up-convs (--> 32x32x128)
        upconv2 = Conv2D(64, (5,5), strides=(1,1), padding='same', name='upconv2',kernel_initializer = myKernel_init, use_bias=False)(upconv2)
        if useBatchNorm == True:
            upconv2 = BatchNormalization(scale=False)(upconv2)
        upconv2 = LeakyReLU(alpha=0.1)(upconv2)
        
        #combine upconv2 + conv2_1 +vdm16up (--> 32x32x(64+1)
        jointLayer2 =  Lambda(lambda x : K.concatenate([x[0],x[1],x[2]],axis=-1),output_shape=(32,32,64+64+1),name='3LayerConcat1')([upconv2,conv2_1,vdm16up])
        
        ## -- fork low resolution vdm (32x32x1) -- ##
        vdm32 = Conv2D(1, (3,3), strides=(1,1), padding='same', name='vdm32',kernel_initializer = myKernel_init, use_bias=False)(jointLayer2)
        #apply to 32x32 image
        out32 = Lambda(applyVDM_wrapper,name='out32',output_shape=(32,32,2), trainable=False,arguments={'densityWeighting':True,'sigma':0.8})([vdm32,input_img32])
        #up-convolve low resolution map
        vdm32up = UpSampling2D(size=(2, 2))(vdm32) # (64x64) that's the 'upconvolution bent arrow in paper
        vdm32up = Conv2D(1, (5,5), strides=(1,1), padding='same', name='vdm32_upconv',kernel_initializer = myKernel_init, use_bias=False)(vdm32up)
        ## -- vdm32up = (64x64x1) -- ##
        
        ##up-conv joint layer with c/4 (-->64x64x32)
        upconv1 = UpSampling2D(size=(2, 2))(jointLayer2) 
        upconv1 = Conv2D(32, (5,5), strides=(1,1), padding='same', name='upconv1',kernel_initializer = myKernel_init, use_bias=False)(upconv1)
        if useBatchNorm == True:
            upconv1 = BatchNormalization(scale=False)(upconv1)
        upconv1 = LeakyReLU(alpha=0.1)(upconv1)
        
        #combine upconv_1 + conv_1 +flow_2up (--> 64x64x(64+1)
        jointLayer1 =  Lambda(lambda x : K.concatenate([x[0],x[1],x[2]],axis=-1),output_shape=(64,64,64+1),name='3LayerConcat2')([upconv1,conv1_1,vdm32up])
        
        ## -- fork low resolution vdm (64x64x1) -- ##
        vdm64 = Conv2D(1, (5,5), strides=(1,1), padding='same', name='vdm64', use_bias=False)(jointLayer1)
        out64 = Lambda(applyVDM_wrapper,name='out64',output_shape=(input_size/2,input_size/2,2), trainable=False,arguments={'densityWeighting':True})([vdm64,input_img64])
        vdm64up = UpSampling2D(size=(2, 2))(vdm64) # (128x 128)
        vdm64up = Conv2D(1, (5,5), strides=(1,1), padding='same', name='vdm64_upconv',kernel_initializer = myKernel_init, use_bias=False)(vdm64up)
        ## - vdm64up = (128x128x1) --- ##
        
        #upconv jointLayer1 --> 128x128x16
        upconv_last=UpSampling2D(size=(2, 2))(jointLayer1)   
        upconv_last = Conv2D(16, (5,5), strides=(1,1), padding='same', name='upconv_last',kernel_initializer = myKernel_init, use_bias=False)(upconv_last)
        
        lastLayer =  Lambda(lambda x : K.concatenate([x[0],x[1],x[2]],axis=-1),output_shape=(128,128,16+7+1),name='threeLayerConcat_final')([upconv_last,conv0_1,vdm64up])        
        
        finalVDM = Conv2D(1, (5,5), strides=(1,1), padding='same', name='finalVDM',kernel_initializer = myKernel_init, use_bias=False)(lastLayer)
        
        #final distortion correction        
        out = Lambda(applyVDM_wrapper,name='final_output',output_shape=(input_size,input_size,2), trainable=False,arguments={'densityWeighting':True})([finalVDM,input_img])
        
        #create model
        self.model = Model(inputs=[input_img,input_img64,input_img32,input_img16],outputs=[out,out64,out32,out16,finalVDM,vdm64,vdm32,vdm16])
        
        print("done generating flownet v3")
        
    def normalize(self, image):
        return image / np.argmax(image)



#-----------------------------------------------#
# not so simple flow net architecture with information flow from contracting layers and 
#-----------------------------------------------#
class SimpleFlowNet_v2(BaseFeatureExtractor):
    """docstring for ClassName"""
    def __init__(self, input_size):
        
        #Glorot normal initializer, also called Xavier normal initializer
        myKernel_init='glorot_normal'
#        myKernel_init='glorot_uniform' #default for conv2d
        
        input_image = Input(shape=(input_size, input_size, 2),name='main_input')
        input_image_64 = Input(shape=(input_size/2, input_size/2, 2),name = 'lowRes_input1')
        input_image_32 = Input(shape=(input_size/4, input_size/4, 2),name='lowRes_input2')
        
        # Layer 0 (e.g. 128x128, c=16) (feed to layer before final prediction)
        conv_0 = Conv2D(8, (7,7), strides=(1,1), padding='same', name='conv_0',kernel_initializer = myKernel_init, use_bias=False)(input_image)
        conv_0 = LeakyReLU(alpha=0.1)(conv_0)
        
        # Layer 1 (e.g. 64x64, c=32)
        conv_1 = Conv2D(32, (5,5), strides=(2,2), padding='same', name='conv_1',kernel_initializer = myKernel_init, use_bias=False)(conv_0)
        conv_1 = LeakyReLU(alpha=0.1)(conv_1)
        
        #a= make_kernel(2)
        #conv_1 = Conv2D(1,(2,2),kernel_initializer = a,use_bias=False,trainable=False)(conv_1)

        #Layer 2 (e.g. 32x32, c = 64)
        conv_2 = Conv2D(64, (3,3), strides=(2,2), padding='same', name='conv_2',kernel_initializer = myKernel_init, use_bias=False)(conv_1)
        conv_2 = LeakyReLU(alpha=0.1)(conv_2)
        
        #Layer 2_1 (e.g. 32x32, c = 64)
        conv_2_1 = Conv2D(64, (3,3), strides=(1,1), padding='same', name='conv_2_1',kernel_initializer = myKernel_init, use_bias=False)(conv_2)
        conv_2_1 = LeakyReLU(alpha=0.1)(conv_2_1)       
        
        #Layer 3 (e.g. 16x16, c = 128)
        conv_3 = Conv2D(128, (3,3), strides=(2,2), padding='same', name='conv_3',kernel_initializer = myKernel_init, use_bias=False)(conv_2_1)
        conv_3 = LeakyReLU(alpha=0.1)(conv_3)
        
        #Layer 3_1 (e.g. 16x16, c = 128)
        conv_3_1 = Conv2D(128, (3,3), strides=(1,1), padding='same', name='conv_3_1',kernel_initializer = myKernel_init, use_bias=False)(conv_3)
        conv_3_1 = LeakyReLU(alpha=0.1)(conv_3_1)        
        
        #Layer 4 (e.g. 8x8, c = 256)
        conv_4 = Conv2D(256, (3,3), strides=(2,2), padding='same', name='conv_4',kernel_initializer = myKernel_init, use_bias=False)(conv_3_1)
        conv_4 = LeakyReLU(alpha=0.1)(conv_4)
        #------ End of ENCODER -------#
        
        #now start the upsampling part (back to 16x16) --- upconvolution = unpooling + convolution + activation
        #c/2 for first up-conv
        upconv_3 = UpSampling2D(size=(2, 2))(conv_4)
        upconv_3 = Conv2D(128, (5,5), strides=(1,1), padding='same', name='upconv_3',kernel_initializer = myKernel_init, use_bias=False)(upconv_3)
        upconv_3 = LeakyReLU(alpha=0.1)(upconv_3)
            
        #add information from contracting part --> (16x16x256)
        jointLayer_3 = Lambda(lambda x : K.concatenate([x[0],x[1]],axis=-1),output_shape=(16,16,256),name='concat_3')([upconv_3,conv_3_1])
            #upconv_3 = Conv2D(128, (1,1), strides=(1,1), padding='same', name='upconv_3_joint', use_bias=False)(jointLayer_3)
            #upconv_3 = LeakyReLU(alpha=0.1)(upconv_3)

        #upsampling of joint layer (--> 32x32x256)
        upconv_2 = UpSampling2D(size=(2, 2))(jointLayer_3) 
        #c/4 for subsequent up-convs (--> 32x32x128)
        upconv_2 = Conv2D(64, (5,5), strides=(1,1), padding='same', name='upconv_2',kernel_initializer = myKernel_init, use_bias=False)(upconv_2)
        upconv_2 = LeakyReLU(alpha=0.1)(upconv_2)
        
        #add conv2 (--> 32x32x(64+64))
        jointLayer_2 = Lambda(lambda x : K.concatenate([x[0],x[1]],axis=-1),output_shape=(32,32,128),name='concat_2')([upconv_2,conv_2_1])

        ## -- fork low resolution vdm (32x32x1) -- ##
        flow_2 = Conv2D(1, (5,5), strides=(1,1), padding='same', name='flow2_map',kernel_initializer = myKernel_init, use_bias=False)(jointLayer_2)
            #flow_2 = Activation('linear',name='flow2_map')(flow_2)
        #apply to 32x32 image
        flow_2_out = Lambda(applyVDM_wrapper,name='flow2_out',output_shape=(32,32,2), trainable=False)([flow_2,input_image_32])
        #up-convolve low resolution map
        flow_2up = UpSampling2D(size=(2, 2))(flow_2) # (64x 64) that's the 'upconvolution bent arrow in paper
        flow_2up = Conv2D(1, (5,5), strides=(1,1), padding='same', name='flow_2_upconv',kernel_initializer = myKernel_init, use_bias=False)(flow_2up)
        ## -- flow_2up = (64x64x1) -- ##
        
        #continue with conv for joint layer2 (still 32 x32) but half the number of channels
        #jointLayer_2= Conv2D(64, (1,1), strides=(1,1), padding='same', name='upconv_2_joint',kernel_initializer = myKernel_init, use_bias=False)(jointLayer_2)
        #upconv_2 = LeakyReLU(alpha=0.1)(upconv_2)
        
        ##up-conv joint layer with c/4 (-->64x64x32)
        upconv_1 = UpSampling2D(size=(2, 2))(jointLayer_2) 
        upconv_1 = Conv2D(32, (5,5), strides=(1,1), padding='same', name='upconv_1',kernel_initializer = myKernel_init, use_bias=False)(upconv_1)
        upconv_1 = LeakyReLU(alpha=0.1)(upconv_1)
        
        #combine upconv_1 + conv_1 +flow_2up (--> 64x64x(64+1)
        jointLayer_1 =  Lambda(lambda x : K.concatenate([x[0],x[1],x[2]],axis=-1),output_shape=(64,64,64+1),name='threeLayerConcat_1')([upconv_1,conv_1,flow_2up])
        
        ## -- fork low resolution vdm (64x64x1) -- ##
        flow_1 = Conv2D(1, (5,5), strides=(1,1), padding='same', name='flow1_map', use_bias=False)(jointLayer_1)
        #flow_1 = Activation('linear',name='flow1_map')(flow_1)
        flow_1_out = Lambda(applyVDM_wrapper,name='flow1_out',output_shape=(input_size/2,input_size/2,2), trainable=False)([flow_1,input_image_64])
        flow_1up = UpSampling2D(size=(2, 2))(flow_1) # (128x 128)
        flow_1up = Conv2D(1, (5,5), strides=(1,1), padding='same', name='flow_1_upconv',kernel_initializer = myKernel_init, use_bias=False)(flow_1up)
        
        #upconv joint Layer 1 --> 128x128x16
        upconv_last=UpSampling2D(size=(2, 2))(jointLayer_1)   
        upconv_last = Conv2D(16, (5,5), strides=(1,1), padding='same', name='upconv_last',kernel_initializer = myKernel_init, use_bias=False)(upconv_last)
        

        lastLayer =  Lambda(lambda x : K.concatenate([x[0],x[1],x[2]],axis=-1),output_shape=(128,128,16+8+1),name='threeLayerConcat_final')([upconv_last,conv_0,flow_1up])
        
        finalVDM = Conv2D(1, (5,5), strides=(1,1), padding='same', name='finalVDM',kernel_initializer = myKernel_init, use_bias=False)(lastLayer)
        #upconv_last = Activation('linear',name='finalVDM')(upconv_last)
        
        #final distortion correction        
        out = Lambda(applyVDM_wrapper,name='final_output',output_shape=(input_size,input_size,2), trainable=False)([finalVDM,input_image])
        
        self.model = Model(inputs=[input_image,input_image_64,input_image_32],outputs=[out,flow_1_out,flow_2_out,finalVDM,flow_1,flow_2])
        
        print("done generating flownet v2")
        
    def normalize(self, image):
        return image / np.argmax(image)      



#-----------------------------------------------#
# simple flow net architecture
#-----------------------------------------------#
class SimpleFlowNet(BaseFeatureExtractor):
    """docstring for ClassName"""
    def __init__(self, input_size):
        
        input_image = Input(shape=(input_size, input_size, 2))
        
        # Layer 1 (e.g. 64x64, c=32)
        conv_1 = Conv2D(16, (5,5), strides=(2,2), padding='same', name='conv_1', use_bias=False)(input_image)
        #conv_1 = BatchNormalization(name='norm_1')(conv_1) #no batch normalization for now
        conv_1 = LeakyReLU(alpha=0.1)(conv_1)
        
        #Layer 2 (e.g. 32x32, c = 64)
        conv_2 = Conv2D(32, (3,3), strides=(2,2), padding='same', name='conv_2', use_bias=False)(conv_1)
        #conv_2 = BatchNormalization(name='norm_2')(conv_2)
        conv_2 = LeakyReLU(alpha=0.1)(conv_2)
        
        #Layer 3 (e.g. 16x16, c = 128)
        conv_3 = Conv2D(64, (3,3), strides=(2,2), padding='same', name='conv_3', use_bias=False)(conv_2)
        #conv_3 = BatchNormalization(name='norm_3')(conv_3)
        conv_3 = LeakyReLU(alpha=0.1)(conv_3)
        
        #Layer 4 (e.g. 8x8, c = 256)
        conv_4 = Conv2D(128, (3,3), strides=(2,2), padding='same', name='conv_4', use_bias=False)(conv_3)
        #conv_4 = BatchNormalization(name='norm_4')(conv_4)
        conv_4 = LeakyReLU(alpha=0.1,name='sniffer')(conv_4)
        
        #now start the upsampling part ("how does it exactly look like?)
        upconv_4=UpSampling2D(size=(2, 2),name='up2d_4')(conv_4) # 16x16 
        upconv_4 = Conv2D(64, (4,4), strides=(1,1), padding='same', name='upconv_4', use_bias=False)(upconv_4)
        upconv_4 = LeakyReLU(alpha=0.1)(upconv_4)

        #upconv_4_1 = Conv2D(64, (3,3), strides=(1,1), padding='same', name='upconv_4_1', use_bias=False)(upconv_4)
        #upconv_4_1 = LeakyReLU(alpha=0.1)(upconv_4_1)

        #continue upsampling
        upconv_3=UpSampling2D(size=(2, 2))(upconv_4) #32 x32
        upconv_3 = Conv2D(32, (4,4), strides=(1,1), padding='same', name='upconv_3', use_bias=False)(upconv_3)
        upconv_3 = LeakyReLU(alpha=0.1)(upconv_3)
        
        #upconv_3_1 = Conv2D(32, (3,3), strides=(1,1), padding='same', name='upconv_3_1', use_bias=False)(upconv_3)
        #upconv_3_1 = LeakyReLU(alpha=0.1)(upconv_3_1)
        
        
        #continue upsampling
        upconv_2=UpSampling2D(size=(2, 2))(upconv_3) #64 x64
        upconv_2 = Conv2D(32, (4,4), strides=(1,1), padding='same', name='upconv_2', use_bias=False)(upconv_2)
        upconv_2 = LeakyReLU(alpha=0.1)(upconv_2)
        
        #upconv_2_1 = Conv2D(16, (3,3), strides=(1,1), padding='same', name='upconv_2_1', use_bias=False)(upconv_2)
        #upconv_2_1 = LeakyReLU(alpha=0.1)(upconv_2_1)
        
        
        #last convolution layer (128x128)
        upconv_1=UpSampling2D(size=(2, 2))(upconv_2) 
        #upconv_1 = Conv2D(16, (4,4), strides=(1,1), padding='same', name='upconv_1', use_bias=False)(upconv_1)
        #upconv_1 = LeakyReLU(alpha=0.1)(upconv_1)
        
        upconv_last = Conv2D(1, (4,4), strides=(1,1), padding='same', name='upconv_last', use_bias=False)(upconv_1)
        upconv_last = Activation('linear',name='vdm_postActivation')(upconv_last)
        
        #final distortion correction        
        out = Lambda(applyVDM_wrapper,name='final_output',output_shape=(input_size,input_size,2), trainable=False)([upconv_last,input_image])
        
        self.model = Model(inputs=input_image,outputs=[ out,upconv_last])
        
        print("done generating flownet")
        
    def normalize(self, image):
        return image / np.argmax(image)