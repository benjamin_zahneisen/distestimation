#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  8 15:13:34 2018

@author: bzahneisen
"""
#from keras.models import Model
#from keras.layers import Reshape, Activation, Conv2D, Input, MaxPooling2D, BatchNormalization, Flatten, Dense, Lambda
#from keras.layers.advanced_activations import LeakyReLU
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
#from matplotlib.pyplot import imshow
#from keras.applications.mobilenet import MobileNet
from keras.layers.merge import concatenate, multiply
from keras.optimizers import SGD, Adam, RMSprop
#from preprocessing import BatchGenerator
from keras.callbacks import EarlyStopping, ModelCheckpoint, TensorBoard
import keras.backend as K

from distEstimatorNet.flow_net_architectures import SimpleFlowNet,SimpleFlowNet_v2, SimpleFlowNet_v3, SimpleFlowNet_v4,SimpleFlowNet_v4_smaller
from distEstimatorNet.SobelLayer import SobelOperator
#import json_utils
import json
import os

class UpDownFlow(object):
    def __init__(self, architecture,
                       input_size,batchNormFlag=False):

        self.input_size = input_size        
        self.architecture   = architecture
        self.sobel = SobelOperator()
        ##########################
        # Make the model
        ##########################
        #input_image     = Input(shape=(self.input_size, self.input_size, 2))
        if architecture == 'SimpleFlowNet':
            self.feature_extractor = SimpleFlowNet(self.input_size) 
        elif architecture == 'SimpleFlowNet_v2':
            self.feature_extractor = SimpleFlowNet_v2(self.input_size)
        elif architecture == 'SimpleFlowNet_v3':
            self.feature_extractor = SimpleFlowNet_v3(self.input_size,batchNormFlag)            
        elif architecture == 'SimpleFlowNet_v4':
            self.feature_extractor = SimpleFlowNet_v4(self.input_size,batchNormFlag) 
        elif architecture == 'SimpleFlowNet_v4_smaller':
            self.feature_extractor = SimpleFlowNet_v4_smaller(self.input_size,batchNormFlag) 
        else:
            raise Exception('Architecture not supported!')

        print(self.feature_extractor.get_output_shape())
        
        #configuration (hyperparameters)
        self.config = {'maxVDMdisplacement':32.0,
                       'w_maxVDM':1000.0,
                       'maxGrad':1.0,
                       'w_sobel':0.00001,
                       'w_tv':0.05,
                       'w_tvimg':0.1,
                       'w_laplace':1}
        
        self.feature_extractor.model.summary()
        self.history = []
  
    ########################## 
    def image_loss(self, y_true, y_pred):
        
        #split output 
        down = y_pred[:,:,:,0]
        up = y_pred[:,:,:,1]
        m = tf.shape(y_pred)[0]
        N =  tf.shape(y_pred)[1] * tf.shape(y_pred)[2]
        N = K.cast(N,'float32')
        m = K.cast(m,'float32')
        
        #sum over total variation (per image) 
        #loss_tvd = (1/N)*tf.reduce_sum(tf.image.total_variation(down))
        #loss_tvu = (1/N)*tf.reduce_sum(tf.image.total_variation(up))
        #loss_tv = loss_tvd + loss_tvu
        
        #mean squared error
        mse = 1/m * (K.sum(K.square(K.abs(down-up)))/N)
    
        loss = 5*mse #+ self.config['w_tvimg']*loss_tv
        
        return loss
 
    ########################## 
    def sobel_loss(self, y_true, y_pred):
        
        #split output 
        down = y_pred[:,:,:,0:1]
        up = y_pred[:,:,:,1:2]
        m = tf.shape(y_pred)[0]
        N =  tf.shape(y_pred)[1] * tf.shape(y_pred)[2]
        N = K.cast(N,'float32')
        m = K.cast(m,'float32')       

        sdown = self.sobel.call(down)
        sup = self.sobel.call(up)
        sob_loss = 1/m * K.sum(K.abs(sdown-sup)/N)
        #sobel loss for data in "upDownDataSingleVol" on the order of 0.1
        
        #sum over total variation (per image) 
        loss_tvd = (1/N)*tf.reduce_sum(tf.image.total_variation(down))
        loss_tvu = (1/N)*tf.reduce_sum(tf.image.total_variation(up))
        loss_tv = loss_tvd + loss_tvu
        
        #mean squared error
        mse = 1/m * (K.sum(K.square(K.abs(down-up)))/N)
        #mse loss for data in "upDownDataSingleVol" on the order of 0.2
        
        loss = 5*mse + self.config['w_sobel']*sob_loss  +self.config['w_tvimg']*loss_tv #loss around 1.5
        return loss 

    ########################## 
    def get_valley_loss(self,VDM,width):
        #valley-shaped loss function "\__/"    
        penalty = K.abs(VDM) - width
        mask = K.greater_equal(penalty,0.0) #bool mask
        mask_float = K.cast(mask,'float32')
        penalty = multiply([penalty,mask_float])
        loss_valley = K.sum(penalty) #do not divide by N to make the penalty relatively large
        
        return loss_valley
    
    ########################## 
    def get_gyVDM_loss(self,VDM,val=1.0):
        #restrict max variation between consecutive pixels (Jacobi matrix?)
        ygrad = self.sobel.call(VDM)
        grad_penalty = K.abs(ygrad) - val
        mask = K.greater_equal(grad_penalty,0.0) #bool mask
        mask_float = K.cast(mask,'float32')
        grad_penalty = multiply([grad_penalty,mask_float])
        loss_grad = 5*K.sum(grad_penalty)
        
        return loss_grad

    ########################## 
    def get_gy2VDM_loss(self,VDM,val=1.0):
        #restrict max variation between consecutive pixels (Jacobi matrix?)
        ygrad = self.sobel.call(VDM)
        y2grad = self.sobel.call(ygrad)
        grad_penalty = K.abs(y2grad) - val
        mask = K.greater_equal(grad_penalty,0.0) #bool mask
        mask_float = K.cast(mask,'float32')
        grad_penalty = multiply([grad_penalty,mask_float])
        loss_grad = 5*K.sum(grad_penalty)
        
        return loss_grad        
        
    ########################## 
    def vdm_loss(self, y_true, y_pred):
        
        #this loss function ensures smooth voxel-displacement-maps by combining
        #valley loss + Jacobian loss with total variation 
        #accounts for lower maxDisplacement for low res images
    
        vdm = y_pred[:,:,:,0:1] #[0:1] because conv2d expects (m,nx,ny,1) 
        m = tf.shape(y_pred)[0]
        N =  tf.shape(y_pred)[1] * tf.shape(y_pred)[2]
        Ny = tf.shape(y_pred)[2]
        N = K.cast(N,'float32')
        m = K.cast(m,'float32')
        Ny = K.cast(Ny,'float32')
    
        #sum over total variation 
        loss_tv = (1/N)*tf.reduce_sum(tf.image.total_variation(vdm))
        #by itself on the order of 0.05 (at start which could mean nothing)
        #during training: goes up to 0.1 or 0.2        

        #new valley-shaped loss function "\__/"
        maxDisplacement = self.config['maxVDMdisplacement'] * (Ny/K.cast(self.input_size,'float32'))
        loss_valley = self.get_valley_loss(vdm,maxDisplacement)/N
        
        #maxGrad2 = 0.1 * (Ny/K.cast(self.input_size,'float32'))
        #loss_grad2 = self.get_gy2VDM_loss(vdm,maxGrad2)/N

        #loss = 1/m *(loss_valley + self.config['w_tv']*loss_tv + self.config['w_laplace']*loss_grad2 )
        
        loss = 1/m * (self.config['w_tv']*loss_tv + self.config['w_maxVDM']*loss_valley)
        
        return loss

    
    ##########################     
    def plot_loss(self,startIdx = 0,fslLoss=None,save=0):
        
        #gather losses
        final_output_loss =[]
        val_final_output_loss=[]
        for l in range(len(self.history)):
            final_output_loss = final_output_loss + self.history[l].history['final_output_loss']
            val_final_output_loss = val_final_output_loss + self.history[l].history['val_final_output_loss']

        # summarize history for loss
        plt.plot(final_output_loss[startIdx:])
        plt.plot(val_final_output_loss[startIdx:])
        plt.title('model loss with lambda='+str(self.config['w_tv']))
        plt.ylabel('loss')
        plt.xlabel('epoch')
        plt.legend(['training loss', 'test loss'], loc='upper left')
        #plt.savefig('loss_every100th_120epochs.png')
        if not fslLoss==None:
            plt.axhline(fslLoss,linewidth=1.5,color='black',linestyle='dashed')
        
        plt.show()
    
    
        return np.array(final_output_loss),np.array(val_final_output_loss)
    
    ##########################    
    def compileModel(self,learning_rate,w2,optimizer='adam'):
        
        if optimizer == 'adam':
            optim = Adam(lr=learning_rate, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
            print("using adam optimizer.")
        else:
            optim = SGD(lr=learning_rate, momentum=0.9, decay=0.0)
            print("using sgd optimizer.")
        
        ##-- flowNet v2 --##
        if self.architecture == 'SimpleFlowNet_v2':
            myLoss={'final_output': self.sobel_loss, 
                     'flow1_out':self.custom_loss,
                     'flow2_out':self.custom_loss, 
                     'finalVDM': self.vdm_loss_tv,
                     'flow1_map': self.vdm_loss64,
                     'flow2_map':self.vdm_loss64
                     }
            myWeights={'final_output': 1.,'flow1_out':1.,'flow2_out':1.,'finalVDM': w2,'flow1_map': w2,'flow2_map': w2,
                       }       
        ##-- flowNet v3 --##
        elif self.architecture == 'SimpleFlowNet_v3':
            myLoss={'final_output': self.image_loss, 
                    'out64':self.image_loss,
                    'out32':self.image_loss,
                    'out16':self.image_loss, 
                    'finalVDM': self.vdm_loss,
                    'vdm64': self.vdm_loss,
                    'vdm32':self.vdm_loss,
                    'vdm16':self.vdm_loss,                    
                    } 
            myWeights={'final_output':1.,'out64':1.,'out32':1.,'out16':1.,'finalVDM':w2,'vdm64':w2,'vdm32':w2,'vdm16':w2}

        ##-- flowNet v4 --##
        elif self.architecture in ['SimpleFlowNet_v4','SimpleFlowNet_v4_smaller']:
            myLoss={'final_output': self.image_loss, 
                    'out64':self.image_loss,
                    'out32':self.image_loss, 
                    'finalVDM': self.vdm_loss,
                    'vdm64': self.vdm_loss,
                    'vdm32':self.vdm_loss,                  
                    } 
            myWeights={'final_output':1.,'out64':1.,'out32':1.,'finalVDM':w2,'vdm64':w2,'vdm32':w2}
                        
        else:
            print("model architecture not known.")
        
        #compile model
        self.feature_extractor.model.compile(loss=myLoss,loss_weights=myWeights,optimizer=optim)
        return True        

    ##########################            
    def predict(self, images):
                
        if self.architecture == 'SimpleFlowNet':
            modelOut,vdmOut = self.feature_extractor.model.predict(images,verbose=1)
            output = (modelOut,vdmOut)
        elif self.architecture == 'SimpleFlowNet_v2':
            modelOut,low_out1,low_out2,vdmOut,vdm_out1,vdm2 = self.feature_extractor.model.predict(images,verbose=1)
            output = (modelOut,low_out1,low_out2,vdmOut,vdm_out1,vdm2)
        elif self.architecture == 'SimpleFlowNet_v3':
            modelOut,out64,out32,out16,vdmOut,vdm64,vdm32,vdm16 = self.feature_extractor.model.predict(images,verbose=1)
            output = (modelOut,out64,out32,out16,vdmOut,vdm64,vdm32,vdm16)
        elif self.architecture in ['SimpleFlowNet_v4','SimpleFlowNet_v4_smaller']:
            modelOut,out64,out32,vdmOut,vdm64,vdm32 = self.feature_extractor.model.predict(images,verbose=1)
            output = (modelOut,out64,out32,vdmOut,vdm64,vdm32)
        else:
            output = []
                
        return output


    ##########################    
    def train(self, train_imgs,     # the list of images to train the model
              valid_imgs, #validation data (can be empty)
              epochs = 50,
              ):
        
        #generate dummy "ground truth"
        y_true, y_trueTest = self.getDummyYtrue(train_imgs,valid_imgs)
        
        earlyStop = EarlyStopping(monitor='loss',min_delta = 0.001,patience=5,verbose=1)
        
        tboard = TensorBoard(log_dir='./logs',histogram_freq=0,batch_size=32,write_graph=True,write_grads=True,write_images=False)
        
        if len(y_trueTest) > 0:
            validData =(valid_imgs,y_trueTest)
            hist = self.feature_extractor.model.fit(x=train_imgs, y=y_true,validation_data=validData, batch_size=32, epochs=epochs, verbose=1 )            
        else:
            hist = self.feature_extractor.model.fit(x=train_imgs, y=y_true, batch_size=32, epochs=epochs, verbose=1)                                         

        self.history.append(hist)
        
        return hist


    ##########################    
    def evaluate(self, input_imgs):     # the list of images to train the model

        #generate dummy "ground truth"
        y_true,a = self.getDummyYtrue(input_imgs,[])
        loss = self.feature_extractor.model.evaluate(x=input_imgs,y=y_true,batch_size=32,verbose=1)
    
        return loss
 
    
    ##########################    
    def getDummyYtrue(self, train_imgs,valid_imgs):
        
        yDummy=[]
        for l in range(len(train_imgs)):
            yDummy.append(np.zeros(train_imgs[l].shape))
        for l in range(len(train_imgs)):
            yDummy.append(np.zeros((train_imgs[l].shape[0],train_imgs[l].shape[1],train_imgs[l].shape[2],1)))        
        
        if len(valid_imgs) > 0:
            yDummyValid=[]
            for l in range(len(valid_imgs)):
                yDummyValid.append(np.zeros(valid_imgs[l].shape))
            for l in range(len(valid_imgs)):
                yDummyValid.append(np.zeros((valid_imgs[l].shape[0],valid_imgs[l].shape[1],valid_imgs[l].shape[2],1)))        
        else:
            yDummyValid =[]
        
        return yDummy,yDummyValid
    
    ########################## 
    def save(self,filename,folder='.'):
        
        #gather losses
        if 'final_output_loss' in self.history[0].history.keys():
            final_output_loss =[]
            for l in range(len(self.history)):
                final_output_loss = final_output_loss + self.history[l].history['final_output_loss']

            #val = np.array(val_final_output_loss)
            np.save(os.path.join(folder,'loss'),final_output_loss)
            #np.save('loss',final_output_loss)

        #gather losses
        if 'val_final_output_loss' in self.history[0].history.keys():
            val_final_output_loss=[]
            for l in range(len(self.history)):
                val_final_output_loss = val_final_output_loss + self.history[l].history['val_final_output_loss']

            #val = np.array(val_final_output_loss)
            np.save(os.path.join(folder,'valLoss'),val_final_output_loss)
        
        
        #save model weights
        self.feature_extractor.model.save_weights(os.path.join(folder,filename))
        
        
        #save architecture
        fname,suffix = filename.split('.')
        json_model=self.feature_extractor.model.to_json()
        full_fname = os.path.join(folder,fname+'.json')
        with open(full_fname, 'w') as outfile:
            json.dump(json_model, outfile)
        
    ########################## 
    def load(self,filename):
        
        self.feature_extractor.model.load_weights(filename)