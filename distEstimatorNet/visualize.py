#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Apr  7 13:52:12 2018

@author: bzahneisen
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import imshow
import keras.backend as K
from keras.utils import plot_model

#import the flow model
from utils import getDatasets,loadNiftiDatasets,_extractTrainingData,_resizeUpDownDatasets
from utils import displayResults,vol2mosaic
from distEstimatorNet.preprocessInput import preprocessInputData
from distEstimatorNet.distEstimator import UpDownFlow

base_folder = '/home/bzahneisen/upDownData'
#base_folder = '/Users/bzahneisen/MRData/upDownData'
datasetsInfo = getDatasets(base_folder)
upDownDatasets = loadNiftiDatasets(base_folder,[datasetsInfo[0]],maxVolsPerDataset = 4)
upDownDatasets_N128 = _resizeUpDownDatasets(upDownDatasets,(128,128))
trainingData = _extractTrainingData(upDownDatasets_N128)

######prepare data
n_train_full,n_trainN64,n_trainN32,n_trainN16 = preprocessInputData(trainingData[:,:,:,:])
inputImgs = [n_train_full,n_trainN64,n_trainN32,n_trainN16]

###############################
#   Construct the model 
###############################
updownflow = UpDownFlow(architecture= 'SimpleFlowNet_v3',input_size = 128,batchNormFlag=True)
updownflow.config['w_tv'] = 0.1
updownflow.config['w_tvimg'] = 0.0001 #very low weighting for TV on corrected images

###############################
#   load pre-trained weights
###############################
updownflow.load('v3_bNormOn_densityWeighting_50epocs.h5')

###############################
#  predict output
###############################
prediction = updownflow.predict(inputImgs)
out,out64,out32,out16,vdm,vdm64,vdm32,vdm16 = prediction

t=5
displayResults(prediction,inputImgs,t)

############################################
# debug end of encoder layer
############################################
def plotLayer(m_model,inputData,name):
    
    #get input layer
    inputNode=m_model.get_layer("main_input")
    outputNode=m_model.get_layer(name)
    layer = K.function([inputNode.input],[outputNode.output])
    layerOut = layer([inputData])[0] #these two lines work

    return layerOut

layer0 = plotLayer(updownflow.feature_extractor.model,n_train_full,'layer0')
fig,ax = plt.subplots(1);ax.imshow(vol2mosaic(layer0[t,:,:,:]))

layer0_1 = plotLayer(updownflow.feature_extractor.model,n_train_full,'layer0_1')
fig,ax = plt.subplots(1);ax.imshow(vol2mosaic(layer0_1[t,:,:,:]))

layer1 = plotLayer(updownflow.feature_extractor.model,n_train_full,'layer1')
fig,ax = plt.subplots(1);ax.imshow(vol2mosaic(layer1[t,:,:,:]))

layer1_1 = plotLayer(updownflow.feature_extractor.model,n_train_full,'layer1_1')
fig,ax = plt.subplots(1);ax.imshow(vol2mosaic(layer1_1[t,:,:,:]))

layer2 = plotLayer(updownflow.feature_extractor.model,n_train_full,'layer2')
fig,ax = plt.subplots(1);ax.imshow(vol2mosaic(layer2[t,:,:,:]))

layer2_1 = plotLayer(updownflow.feature_extractor.model,n_train_full,'layer2_1')
fig,ax = plt.subplots(1);ax.imshow(vol2mosaic(layer2_1[t,:,:,:]))

layer3 = plotLayer(updownflow.feature_extractor.model,n_train_full,'layer3')
fig,ax = plt.subplots(1);ax.imshow(vol2mosaic(layer3[t,:,:,:]))