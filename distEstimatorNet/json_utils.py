#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  1 15:34:02 2018

@author: bzahneisen
"""

import json
#json_model=updownflow.feature_extractor.model.to_json()
#with open('data.txt', 'w') as outfile:
#    json.dump(data, outfile)

def trim_model(json_model):
    #input model encoded as json string
    model = json.loads(json_model)
    
    #model is a dict with 4 entries 'keras_version','class_name','config','backend'
    modelConfig = model['config']
    
    #remove all but one input layer
    #modelConfig['input_layers']
    while(len(modelConfig['input_layers'])>1):
        modelConfig['input_layers'].pop()
    
    #only keep one output layer with the voxel displacement map
    modelConfig['output_layers'] = [modelConfig['output_layers'][3]]
    
    # 'layers' has all the layer information
    layers = modelConfig['layers'] #list (e.g. length=64) of dicts
    new_layers = [l for l in layers if l['name'] not in ('input64','input32','final_output','out64','out32')]
    
    
    modelConfig['layers'] = new_layers
    
    model['config'] = modelConfig
    
    return json.dumps(model,separators=(', ',': '))

            