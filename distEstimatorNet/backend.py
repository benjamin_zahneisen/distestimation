#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 23 09:34:34 2018

@author: bzahneisen
"""

import tensorflow as tf
import keras.backend as K
from keras import backend as K
import numpy as np
#from distEstimatorNet.warpImages import unwarp_Cubic


############################################
# resize keras tensors (unwarp using 1d cubic interpolation)
############################################
def resize(X,newSize):
    """Description: 
        Input: list/tuple of keras tensors of equal size [voxelDisplacementMap, Image]
        Image.shape = (m,Nx,Ny,c)
    """


    #get dimensions 
    batch_size_tf=tf.shape(X)[0] 
    Nx_tf = tf.shape(X)[1]
    Ny_tf = tf.shape(X)[2]
    Nc_tf = tf.shape(X)[3]
    
    batchSize_float = tf.cast(batch_size_tf, 'float32')
    Nx_float = tf.cast(Nx_tf, 'float32')
    Ny_float = tf.cast(Ny_tf, 'float32')
    batchSize_int = tf.cast(batch_size_tf, 'int32')
    Nx_int = tf.cast(Nx_tf, 'int32')
    Ny_int = tf.cast(Ny_tf, 'int32')


############################################
# unwarpImagesTF is lambda layer fun in keras
# call 'pure' python unwarping using tf.py_func
# input X = [vdm,upDownInput]  
# vdm = voxel displacement map (m x Nx x Ny)
# upDownInput = m x Nx x Ny x 2
#return unwarped epis (m x Nx x Ny x 2)
############################################
def unwarpImagesTF(X):
    
    vdm,upDownInput = X 
    
    #get dimensions 
    batch_size_tf=tf.shape(vdm)[0] 
    Nx_tf = tf.shape(vdm)[1]
    Ny_tf = tf.shape(vdm)[2]
    
    down    = upDownInput[:,:,:,0] #still keras-tensors
    up      = upDownInput[:,:,:,1]
    vdmDown = -vdm
    vdmUp   = vdm 
    
    downCorr = tf.py_func(unwarp_Cubic,[vdmDown,down],tf.float32)
    upCorr = tf.py_func(unwarp_Cubic,[vdmUp,up],tf.float32)
    
    downCorr = K.reshape(downCorr,[batch_size_tf,Nx_tf,Ny_tf,1])
    upCorr = K.reshape(upCorr,[batch_size_tf,Nx_tf,Ny_tf,1])
    unwrpX = K.concatenate([downCorr,upCorr],axis=-1) 
    
    #return Keras tensor
    return unwrpX



############################################
# resize keras tensors (unwarp using 1d cubic interpolation)
    # 1. applyVDM (blipDown)
    # 2. applyVDM (blipUp)
    # return cat(down_corr,up_corr)
############################################
def applyVDM_wrapper(X,densityWeighting=True,sigma = 0.8):
    
    #
    from distEstimatorNet.backend import applyVDM_compact
    
    vdm,upDownInput = X 
    
    #Lambda layer that splits input (looks like python but is still a symbolic tensorflow operation)
    input_down = upDownInput[:,:,:,0]
    input_up = upDownInput[:,:,:,1]
    vdmDown = vdm
    vdmUp = -vdm

    #Lambda layer that performs the displacement
    #singleDown = Lambda(applyVDM,name='vdmDown',output_shape=(input_size,input_size,1), trainable=False)([vdmDown,input_down])
    #singleUp = Lambda(applyVDM,name='vdmUp',output_shape=(input_size,input_size,1), trainable=False)([vdmUp,input_up])
    singleDown = applyVDM_compact([vdmDown,input_down],densityWeighting,sigma)
    singleUp = applyVDM_compact([vdmUp,input_up],densityWeighting,sigma)
        
    #do something
    #    out = Lambda(lambda x : K.concatenate([x[0],x[1]],axis=-1),output_shape=(input_size,input_size,2),name='finalConcat')([singleDown,singleUp])
    out = K.concatenate([singleDown,singleUp],axis=-1)   
    
    
    return out


############################################
# applyVDM with keras tensors (unwarp using 1d cubic interpolation)
############################################    
def applyVDM(X):
    """Description: 
        Input: list/tuple of keras tensors of equal size [voxelDisplacementMap, Image]
        Image.shape = (m,Nx,Ny,1)
        voxelDisplacementMap.shape = (m,Nx,Ny,1) where each pixel encodes the spatial shift (in voxels or fraction therefore) along the 2nd dimension of input image.
        
        Output = distortion corrected image (1d cubic interpolation along y)
        
    """
    
    vdm,epi = X
    
    #get dimensions 
    batch_size_tf=tf.shape(vdm)[0] 
    Nx_tf = tf.shape(vdm)[1]
    Ny_tf = tf.shape(vdm)[2]

    batchSize_float = tf.cast(batch_size_tf, 'float32')
    Nx_float = tf.cast(Nx_tf, 'float32')
    Ny_float = tf.cast(Ny_tf, 'float32')
    batchSize_int = tf.cast(batch_size_tf, 'int32')
    Nx_int = tf.cast(Nx_tf, 'int32')
    Ny_int = tf.cast(Ny_tf, 'int32')


    #y-grid of new image
    y_linspace = tf.linspace(0.0,Ny_float-1.0, Ny_int)      
    y0 = tf.reshape(y_linspace,[1,1,Ny_int])
    y0 = tf.tile(y0,[batchSize_int,Nx_int,1]) #[m,Nx,Ny]
    
    #make image tensor array shaped
    vdm_flat = K.reshape(vdm,[-1])
    epi_flat = K.reshape(epi,[-1])
    y0 = tf.reshape(y0,[-1]) # m*Nx copies of [0,127] for Ny=128
    
    
    #inverse map
    uprime = tf.subtract(y0,vdm_flat)
    
    #get two points left and 2 points right of uprime
    ul = tf.floor(uprime)
    ul = tf.cast(ul,'int32')
    ul2 = tf.add(ul,-1)
    ur = tf.add(ul, 1)
    ur2 =tf.add(ul,2)
    

    #restrict index 
    ul  = tf.clip_by_value(ul,0,Ny_int-1)
    ul2 = tf.clip_by_value(ul2,0,Ny_int-1)
    ur  = tf.clip_by_value(ur,0,Ny_int-1)
    ur2 = tf.clip_by_value(ur2,0,Ny_int-1)
    
    # y= distance between coordinate to be interpolated and grid point to the left
    y = tf.subtract(uprime,tf.cast(ul,'float32'))
    
    
    #calculate weight function
#    i = tf.constant(0)
#    imax=tf.shape(vdm_flat)[0] 
#    #while_condition = lambda i: tf.less(i, imax)
#    #another approach
#    def body(i,outputs): 
#        # do something here which you want to do in your loop
#        bin_i = tf.equal(ul,i)
#        ix = tf.where(bin_i) #should return index where b==True as 2d-Tensor. 
#        #A Tensor with shape (num_true, dim_size(condition)).
#        #num_true_i = c[0]
#        indexTrue_i = ix
#        #vals = tf.gather(y,indexTrue_i)
#        vals=tf.constant(0.1)
#        wi = tf.reduce_mean(vals,keepdims=False)
#        wi = tf.minimum(tf.cast(tf.shape(ix),'float32'),wi)
#        outputs = outputs.write(i, wi)
#        i += 1
#        return i,outputs
#    
#    outputs = tf.TensorArray(dtype=tf.float32, infer_shape=True, size=imax, 
#                     dynamic_size=False)
#    tmp = lambda i, *_: tf.less(i, imax)
#    _, outputs = tf.while_loop(tmp, body,[i,outputs],parallel_iterations=1)
#
#    w = outputs.concat()
#    
#    #apply weighting
#    epi_flat = tf.multiply(epi_flat,w)
        
    #generate x-offset
    flat_image_dimensions = tf.multiply(Nx_float,Ny_float)
    a = tf.subtract(flat_image_dimensions,Ny_float)
    x_linspace = tf.linspace(0.0,a, Ny_int)
    x0 = tf.reshape(x_linspace,[1,Nx_int,1])
    #y0 = tf.tile(y0,(batch_size_tf,Nx_tf,1)) #[m,Nx,Ny]
    x0 = tf.tile(x0,[batchSize_int,1,Ny_int]) #[m,Nx,Ny]
    x0 = tf.reshape(x0,[-1])
    
    #batch offset
    pixels_batch =  tf.linspace(0.0,batchSize_float-1.0,batchSize_int)*flat_image_dimensions 
    pixels_batch = tf.reshape(pixels_batch,[batchSize_int,1,1])
    pixels_batch = tf.tile(pixels_batch,[1,Nx_int,Ny_int])    
    pixels_batch_flat = tf.reshape(pixels_batch,[-1])
    
    #indices are now integers
    ul  = tf.cast(ul,'int32') 
    ul2 = tf.cast(ul2,'int32')
    ur  = tf.cast(ur,'int32')
    ur2 = tf.cast(ur2,'int32')
    x0 = tf.cast(x0,'int32')
    y_base = tf.cast(pixels_batch_flat,'int32')
    
    #all batches and all images are one long pixel array
    #y_base and x0 'index' into the array to select one 'y-line'
    ul_indices = ul + y_base + x0
    ul2_indices = ul2 + y_base + x0
    ur_indices = ur + y_base +x0
    ur2_indices = ur2 + y_base +x0
    y0_indices = y0 + y_base + x0 #[0...,127,128,...] increasing indices ?
    
    #now is a good place to estimate the density on the whole vector
    y0_grid = tf.tile(y0_indices,[1,tf.shape(y0_indices)])
    b = tf.equal(ul_indices,y0_grid)
    
    #restrict index
    maxIndex = tf.multiply(flat_image_dimensions,batchSize_float)
    maxIndex_int = tf.cast(maxIndex,'int32')
    ul_indices  = tf.clip_by_value(ul_indices,0, maxIndex_int-1)
    ul2_indices = tf.clip_by_value(ul2_indices,0, maxIndex_int-1)
    ur_indices  = tf.clip_by_value(ur_indices,0, maxIndex_int-1)
    ur2_indices = tf.clip_by_value(ur2_indices,0, maxIndex_int-1)
    
    cubicTerm = (-0.5*tf.gather(epi_flat,ul2_indices) + 3/2*tf.gather(epi_flat,ul_indices) -3/2*tf.gather(epi_flat,ur_indices) +1/2*tf.gather(epi_flat,ur2_indices))*tf.pow(y,3)
    quadraticTerm = (tf.gather(epi_flat,ul2_indices) - 5/2*tf.gather(epi_flat,ul_indices) +2*tf.gather(epi_flat,ur_indices) -1/2*tf.gather(epi_flat,ur2_indices))*tf.pow(y,2)
    linearTerm = (-1/2*tf.gather(epi_flat,ul2_indices) + 1/2*tf.gather(epi_flat,ur_indices))*(y)
    zeroTerm = tf.gather(epi_flat,ul_indices)
    
    unwarp_epi = cubicTerm + quadraticTerm + linearTerm + zeroTerm
    
    unwarp_epi = K.reshape(unwarp_epi,[batchSize_int,Nx_int,Ny_int,1])
    
    return unwarp_epi


############################################
# applyVDM with keras tensors (unwarp using 1d cubic interpolation)
############################################    
def applyVDM_compact(X,densityWeighting=True,sigma=0.8):
    """Description: 
        Input: list/tuple of keras tensors of equal size [voxelDisplacementMap, Image]
        Image.shape = (m,Nx,Ny,1)
        voxelDisplacementMap.shape = (m,Nx,Ny,1) where each pixel encodes the spatial shift (in voxels or fraction therefore) along the 2nd dimension of input image.
        
        Output = distortion corrected image (1d cubic interpolation along y)
        
    """  
    vdm,epi = X
    
    #get dimensions 
    batch_size_tf=tf.shape(vdm)[0]
    Nx_tf = tf.shape(vdm)[1]
    Ny_tf = tf.shape(vdm)[2]

    batchSize_float = tf.cast(batch_size_tf, 'float32')
    Nx_float = tf.cast(Nx_tf, 'float32')
    Ny_float = tf.cast(Ny_tf, 'float32')
    batchSize_int = tf.cast(batch_size_tf, 'int32')
    Nx_int = tf.cast(Nx_tf, 'int32')
    Ny_int = tf.cast(Ny_tf, 'int32')

    #make image tensor array shaped
    vdm_flat = K.reshape(vdm,[-1])
    epi_flat = K.reshape(epi,[-1])
    Npixels = tf.shape(vdm_flat)[0] #all pixels (batch*Nx*Ny)
    #y0 = [0,...,127,128,...,255,....]
    y0 = tf.linspace(0.0,tf.cast(Npixels,'float32')-1.0,Npixels)

    #circular periodicity (not yet)
    #y-grid of new image
    #y_linspace = tf.linspace(0.0,Ny_float-1.0, Ny_int)      
    #y0 = tf.reshape(y_linspace,[1,1,Ny_int])
    #y0 = tf.tile(y0,[batchSize_int,Nx_int,1]) #[m,Nx,Ny]
    #y0 = tf.reshape(y0,[-1]) # m*Nx copies of [0,127] for Ny=128
    #z = tf.zeros(Npixels,dtype='int32')
    #maxIdx = (Ny_int-1)*tf.ones_like(ul)
    #circPlus = tf.where(ul<0,z,maxIdx)
    #circMinus = tf.where(ul>(Ny_int-1),z,maxIdx)
    #ul = ul + circPlus
    #ul = ul - circMinus #repeat for all coordinates
    
    #inverse map
    uprime = tf.subtract(y0,vdm_flat)
    
    #get two points left and 2 points right of uprime
    ul  = tf.cast(tf.floor(uprime),'int32')
    ul2 = tf.add(ul,-1)
    ur  = tf.add(ul, 1)
    ur2 = tf.add(ul, 2)
    
    #restrict index
    maxIndex_int = tf.shape(epi_flat)
    ul  = tf.clip_by_value(ul,0, maxIndex_int-1)
    ul2 = tf.clip_by_value(ul2,0, maxIndex_int-1)
    ur  = tf.clip_by_value(ur,0, maxIndex_int-1)
    ur2 = tf.clip_by_value(ur2,0, maxIndex_int-1)    
    
    # y= distance between coordinate to be interpolated and grid point to the left
    y = tf.subtract(uprime,tf.cast(ul,'float32')) # y between 0 and 1
    
    if densityWeighting == True:
        #now is a good place to estimate the density on the whole vector    
        numSegments = tf.shape(y)[0]
        wl = tf.unsorted_segment_sum(1-y,ul,numSegments,name='wl')
        wr = tf.unsorted_segment_sum(y,ur,numSegments,name='wr')
    
        #sort of linear interpolation
        w = (wl+wr)
        #gaussian shaped filter(kernel_size=3,sigma default = 0.8)
        if(sigma > K.epsilon()):
            idx=np.array([-1,0,1])
            g=(np.exp(-idx**2/(2*sigma**2))).astype('float32')
            filters = tf.constant(g,shape=[3,1,1])/np.sum(g)
            w = tf.reshape(w,(batch_size_tf,Nx_tf*Nx_tf,1))
            w=tf.nn.conv1d(w,filters,stride=1,padding='SAME')
            w = tf.reshape(w,[-1])    
    
        mask = tf.greater(w,0.0)
        w = tf.clip_by_value(w,K.epsilon(),10.0)    
        w = tf.multiply(1/w,tf.cast(mask,'float32'))
    
        #apply density weighting to image before interpolation
        epi_flat = tf.multiply(epi_flat,w)
    
    cubicTerm = (-0.5*tf.gather(epi_flat,ul2) + 3/2*tf.gather(epi_flat,ul) -3/2*tf.gather(epi_flat,ur) +1/2*tf.gather(epi_flat,ur2))*tf.pow(y,3)
    quadraticTerm = (tf.gather(epi_flat,ul2) - 5/2*tf.gather(epi_flat,ul) +2*tf.gather(epi_flat,ur) -1/2*tf.gather(epi_flat,ur2))*tf.pow(y,2)
    linearTerm = (-1/2*tf.gather(epi_flat,ul2) + 1/2*tf.gather(epi_flat,ur))*(y)
    zeroTerm = tf.gather(epi_flat,ul)
    
    unwarp_epi = cubicTerm + quadraticTerm + linearTerm + zeroTerm
    
    unwarp_epi = K.reshape(unwarp_epi,[batchSize_int,Nx_int,Ny_int,1])
    
    return unwarp_epi


def memmap2ndarray(x):
    dim = x.shape
    y = np.zeros(dim)
    for i in range(dim[0]):
        for j in range(dim[1]):
            for k in range(dim[2]):
                y[i,j,k] = float(x[i,j,k])                
    return y