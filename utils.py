#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 28 10:33:06 2018

@author: bzahneisen
"""

import os
import numpy as np
import nibabel as nib
#import cv2
import scipy.ndimage as sc
import matplotlib.pyplot as plt
from matplotlib.pyplot import imshow
from distEstimatorNet.preprocessInput import normalizeInputVol


def displayFSLResults(fslCorr,inputImgs,unCorr,t):
    exponent = 1
    
    fig,ax=plt.subplots(nrows=3,ncols=3)
    ax[0,0].imshow(np.flipud(np.transpose(fslCorr[t,:,:,1])),cmap='gray')
    ax[0,0].title.set_text('FSL_down')
    ax[0,1].imshow(np.flipud(np.transpose(fslCorr[t,:,:,0])),cmap='gray')
    ax[0,1].title.set_text('FSL_up')
    diff = np.power(fslCorr[t,:,:,1] - fslCorr[t,:,:,0],exponent)
    dmin = 1.1*np.min(diff)
    dmax = 0.9*np.max(diff)
    ax[0,2].imshow(np.flipud(np.transpose(diff)),vmin=dmin,vmax=dmax)
    ax[0,2].title.set_text('diff corr')

    ax[1,0].imshow(np.flipud(np.transpose(inputImgs[t,:,:,0])),cmap='gray')
    ax[1,0].title.set_text('down')
    ax[1,1].imshow(np.flipud(np.transpose(inputImgs[t,:,:,1])),cmap='gray')
    ax[1,1].title.set_text('up')
    diffAI = np.power(inputImgs[t,:,:,0] - inputImgs[t,:,:,1],exponent)
    ax[1,2].imshow(np.flipud(np.transpose(diffAI)),vmin=dmin,vmax=dmax)
    ax[1,2].title.set_text('diff AI')
    
    ax[2,0].imshow(np.flipud(np.transpose(unCorr[t,:,:,0])),cmap='gray')
    ax[2,0].title.set_text('down0')
    ax[2,1].imshow(np.flipud(np.transpose(unCorr[t,:,:,1])),cmap='gray')
    ax[2,1].title.set_text('up0')
    diff0 = np.power(unCorr[t,:,:,0] - unCorr[t,:,:,1],exponent)
    ax[2,2].imshow(np.flipud(np.transpose(diff0)),vmin=dmin,vmax=dmax)
    ax[2,2].title.set_text('diff uncorr')

    #disable all axes
    for n in range(3):
        for m in range(3):
            ax[n,m].axes.axis('off')

def displayResults(prediction,inputImgs,t,lowRes=False):
    
    out,out64,out32,vdm,vdm64,vdm32 = prediction
    train_full,trainN64,trainN32 = inputImgs
    
    
    exponent = 1
    
    fig,ax=plt.subplots(nrows=2,ncols=4)
    ax[0,0].imshow(np.flipud(np.transpose(out[t,:,:,0])),cmap='gray')
    ax[0,0].title.set_text('down_corr')
    ax[0,1].imshow(np.flipud(np.transpose(out[t,:,:,1])),cmap='gray')
    ax[0,1].title.set_text('up_corr')
    diff = np.power(out[t,:,:,0] - out[t,:,:,1],exponent)
    dmin = 1.1*np.min(diff)
    dmax = 0.9*np.max(diff)
    ax[0,2].imshow(np.flipud(np.transpose(diff)),vmin=dmin,vmax=dmax)
    ax[0,2].title.set_text('diff corr')
    ax[0,3].imshow(np.flipud(np.transpose(vdm[t,:,:,0])))
    ax[0,3].title.set_text('vdm')
    
    ax[1,0].imshow(np.flipud(np.transpose(train_full[t,:,:,0])),cmap='gray')
    ax[1,0].title.set_text('down')
    ax[1,1].imshow(np.flipud(np.transpose(train_full[t,:,:,1])),cmap='gray')
    ax[1,1].title.set_text('up')
    diffOrg = np.power(train_full[t,:,:,0] - train_full[t,:,:,1],exponent)
    ax[1,2].imshow(np.flipud(np.transpose(diffOrg)),vmin=dmin,vmax=dmax)
    ax[1,2].title.set_text('diff0')
      

    #disable all axes
    for n in range(2):
        for m in range(4):
            ax[n,m].axes.axis('off')

    if lowRes==True:
        #2nd plot
        fig2,ax2=plt.subplots(nrows=2,ncols=5)
        ax2[0,0].imshow(np.flipud(np.transpose(out64[t,:,:,0])),cmap='gray')
        ax2[0,0].title.set_text('N64 dwn corr')
        ax2[0,0].set_ylabel('sdf')
        #ax2[0,0].axes.axis('off')  
        ax2[0,1].imshow(np.flipud(np.transpose(out64[t,:,:,1])),cmap='gray')
        ax2[0,1].title.set_text('up corr')
        diff = out64[t,:,:,0] - out64[t,:,:,1]
        dmin = 1.1*np.min(diff)
        dmax = 0.9*np.max(diff)
        ax2[0,2].imshow(np.flipud(np.transpose(diff)),vmin=dmin,vmax=dmax)
        ax2[0,2].title.set_text('diff corr')
        diff0 = trainN64[t,:,:,0] - trainN64[t,:,:,1]
        ax2[0,3].imshow(np.flipud(np.transpose(diff0)),vmin=dmin,vmax=dmax)
        ax2[0,3].title.set_text('diff0')
        ax2[0,4].imshow(np.flipud(np.transpose(vdm64[t,:,:,0])))
        ax2[0,4].title.set_text('vdm')
    
        ax2[1,0].imshow(np.flipud(np.transpose(out32[t,:,:,0])),cmap='gray')
        ax2[1,0].title.set_text('N32 down corr')
        ax2[1,1].imshow(np.flipud(np.transpose(out32[t,:,:,1])),cmap='gray')
        diff = out32[t,:,:,0] - out32[t,:,:,1]
        dmin = 1.1*np.min(diff)
        dmax = 0.9*np.max(diff)
        ax2[1,2].imshow(np.flipud(np.transpose(diff)),vmin=dmin,vmax=dmax)
        diff0 = trainN32[t,:,:,0] - trainN32[t,:,:,1]
        ax2[1,3].imshow(np.flipud(np.transpose(diff0)),vmin=dmin,vmax=dmax)
        ax2[1,4].imshow(np.flipud(np.transpose(vdm32[t,:,:,0])))

        #disable all axes
        for n in range(2):
            for m in range(5):
                ax2[n,m].axes.axis('off')

            
#in base folder each dataset is in a subfolder 00001,0002,...
#each subfolder contains one "up" and one "down" folder 
def getDatasets(base_folder):
    
    allFolders = os.listdir(base_folder)
    
    #sort folders according to numbers
    allFolders = sorted(allFolders)    
    
    datasetsInfo =[]
    
    for f in allFolders:
        if f.startswith('.'):
            print(f + ' ignored.')
        else:
            fup = [f for f in os.listdir(os.path.join(base_folder,f+"/up")) if f.endswith('.gz')] 
            fdown = [f for f in os.listdir(os.path.join(base_folder,f+"/down")) if f.endswith('.gz')] 
            dataset={'id':f,'name_up':fup,'name_down':fdown,'hasFSL':0}
            datasetsInfo.append(dataset)
        
    return datasetsInfo

def memmap2ndarray(x):
    dim = x.shape
    y = np.zeros(dim)
    for i in range(dim[0]):
        for j in range(dim[1]):
            for k in range(dim[2]):
                y[i,j,k] = float(x[i,j,k])                
    return y

def loadNiftiVol(fname):
    
    vol_nii = nib.load(fname)
    vol = vol_nii.get_data()
    #vol = memmap2ndarray(vol)
    return vol

#load up and down volumes 
def loadNiftiDatasets(base_folder,datasetsInfo,maxVolsPerDataset = 1,volInc = 2):
    
    upDownDatasets =[]
    #maxVolsPerDataset = 3
    
    #each item in upDownDatasets comes from one subject-folder
    for dataset in datasetsInfo:
        fname = dataset['id']+'/'+'up'+'/'+dataset['name_up'][0]
        Vup = loadNiftiVol(os.path.join(base_folder,fname))
        fname = dataset['id']+'/'+'down'+'/'+dataset['name_down'][0]
        Vdown = loadNiftiVol(os.path.join(base_folder,fname))
        dim = len(Vdown.shape) #can be 3d or 4d        
        Nsamples = np.prod(Vup.shape[2:4])
        if dim == 3:
            Nx,Ny,Ns = Vup.shape
            Vup = np.reshape(Vup,(Nx,Ny,Ns,1),'F')
            Vdown = np.reshape(Vdown,(Nx,Ny,Ns,1),'F')
            upDown={'file':dataset,'Nsamples':Nsamples,'up':Vup,'down':Vdown,'dims':Vdown.shape,'orgDims':Vdown.shape,'dim':dim}
        else:
            Vup = Vup[:,:,:,0:maxVolsPerDataset:volInc]
            Vdown = Vdown[:,:,:,0:maxVolsPerDataset:volInc]
            Nsamples = np.prod(Vup.shape[2:4])
            upDown={'file':dataset,'Nsamples':Nsamples,'up':Vup,'down':Vdown,'dims':Vdown.shape,'orgDims':Vdown.shape,'dim':dim}
        upDownDatasets.append(upDown)
    
    return upDownDatasets

#load up and down volumes 
def loadFSLcorrectedDatasets(base_folder,datasetsInfo,maxVolsPerDataset = 1,volInc = 2):
    
    fsl_upDownDatasets =[]
    
    #each item in upDownDatasets comes from one subject-folder
    for dataset in datasetsInfo:
        fname = dataset['id']+'/'+'fsl'+'/'+'Vout.nii.gz'
        Vfsl = loadNiftiVol(os.path.join(base_folder,fname))
        #fname = dataset['id']+'/'+'down'+'/'+dataset['name_down'][0]
        #Vdown = loadNiftiVol(os.path.join(base_folder,fname))
        dim = len(Vfsl.shape) #can be 3d or 4d        
        Nsamples = np.prod(Vfsl.shape[2:4])
        if dim == 3:
            Nx,Ny,Ns = Vup.shape
            Vup = np.reshape(Vup,(Nx,Ny,Ns,1),'F')
            Vdown = np.reshape(Vdown,(Nx,Ny,Ns,1),'F')
            upDown={'file':dataset,'Nsamples':Nsamples,'up':Vup,'down':Vdown,'dims':Vdown.shape,'orgDims':Vdown.shape,'dim':dim}
        else:
            Vup = Vfsl[:,:,:,1]
            Nx,Ny,Ns = Vup.shape
            Vdown = Vfsl[:,:,:,0]
            Vup = np.reshape(Vup,(Nx,Ny,Ns,1),'F')
            Vdown = np.reshape(Vdown,(Nx,Ny,Ns,1),'F')
            Nsamples = np.prod(Vup.shape[2:4])
            upDown={'file':dataset,'Nsamples':Nsamples,'up':Vup,'down':Vdown,'dims':Vdown.shape,'orgDims':Vdown.shape,'dim':dim}
        fsl_upDownDatasets.append(upDown)
    
    return fsl_upDownDatasets


def _extractTrainingData(upDownDatasets,normalize=True,removeEmptySlices=True):
    
    Nx,Ny = upDownDatasets[0]['dims'][0:2]
    m = 0
    for l in range(len(upDownDatasets)):
        m = m + upDownDatasets[l]['Nsamples']
        
    #trainData = np.zeros((m,Nx,Ny,2))
    ListOfDatasets = []
    for l in range(len(upDownDatasets)):
        
        NvolsInSubject = upDownDatasets[l]['up'].shape[3]
        Nslices= upDownDatasets[l]['up'].shape[2]
        #vols = np.zeros((upDownDatasets[l]['Nsamples'],Nx,Ny,2))

        #can be single or multiple volumes
        vols_up=upDownDatasets[l]['up']
        vols_down=upDownDatasets[l]['down']
        vol_upDown = np.zeros((Nx,Ny,Nslices,2))
        vols = np.zeros((Nx,Ny,Nslices,NvolsInSubject,2))
        
        for t in range(vols_up.shape[3]):
            #one up-down vol from one subject
            vol_upDown[:,:,:,0] = vols_up[:,:,:,t]
            vol_upDown[:,:,:,1] = vols_down[:,:,:,t]
            #normalize each volume separately
            if normalize==True:
                vol_upDown,dummy = normalizeInputVol(vol_upDown)
        
            vols[:,:,:,t,:] = vol_upDown
        
        #combine slices and volumes
        #b= np.reshape(vols,(Nx,Ny,upDownDatasets[l]['Nsamples']),'F')
        #tmp = np.transpose(np.reshape(upDownDatasets[l]['up'],(Nx,Ny,upDownDatasets[l]['Nsamples']),'F'),(2,0,1))
        #vol[:,:,:,0] = np.transpose(b,(2,0,1))
        #tmp = np.transpose(np.reshape(upDownDatasets[l]['down'],(Nx,Ny,upDownDatasets[l]['Nsamples']),'F'),(2,0,1))    
        #vol[:,:,:,1] = tmp
        
        #combine slices and volumes
        vol = np.reshape(vols,(Nx,Ny,upDownDatasets[l]['Nsamples'],2))
        vol = np.transpose(vol,(2,0,1,3))
        
        ListOfDatasets.append(vol)
        
    trainData = np.concatenate(ListOfDatasets,axis=0)
    
    if removeEmptySlices==True:
        #check for empty slices
        d = np.sum(trainData,(1,2,3))
        NotEmpty = d > 1
        trainData = trainData[NotEmpty,:,:,:]   
    
    return trainData
 
       
def _resizeUpDownDatasets(upDownDatasets,newSize):
    
    for l in range(len(upDownDatasets)):
        #get slices and volumes for each dataset    
        Nslices = upDownDatasets[l]['dims'][2]
        Nvols =upDownDatasets[l]['dims'][3]
        tmpUp = np.zeros((newSize[0],newSize[1],Nslices,Nvols))
        tmpDown = np.zeros((newSize[0],newSize[1],Nslices,Nvols))
        for sl in range(Nslices):
            for t in range(Nvols):
                if newSize[0] == 64:
                    a=1
                    #fimgUp = cv2.GaussianBlur(upDownDatasets[l]['up'][:,:,sl,t],(5,5),2)
                    #fimgDown = cv2.GaussianBlur(upDownDatasets[l]['down'][:,:,sl,t],(5,5),2)
                elif newSize[0] == 32:
                    a=1
                    #fimgUp = cv2.GaussianBlur(upDownDatasets[l]['up'][:,:,sl,t],(5,5),4)
                    #fimgDown = cv2.GaussianBlur(upDownDatasets[l]['down'][:,:,sl,t],(5,5),4)
                else:
                    fimgUp =  upDownDatasets[l]['up'][:,:,sl,t]
                    fimgDown = upDownDatasets[l]['down'][:,:,sl,t]
                ##### 
                #tmpUp[:,:,sl,t] = cv2.resize(fimgUp,newSize)
                #tmpDown[:,:,sl,t] = cv2.resize(fimgDown,newSize)
                tmpUp[:,:,sl,t] = sc.zoom(fimgUp,newSize[0]/fimgUp.shape[0])
                tmpDown[:,:,sl,t] = sc.zoom(fimgDown,newSize[0]/fimgDown.shape[0])

        upDownDatasets[l]['up']=tmpUp
        upDownDatasets[l]['down']=tmpDown
        upDownDatasets[l]['dims'] = (newSize[0],newSize[1],Nslices,Nvols)
        
        
    return upDownDatasets


def vol2mosaic(V):
    Nx,Ny,Nz = V.shape
    
    Nx = int(Nx)
    Ny = int(Ny)
    Nz= int(Nz)
    tiles = int(np.ceil(np.sqrt(Nz)))
    new = np.zeros((int(Nx*tiles),int(Ny*tiles)))
    c = 0
    for x in range(tiles):
        for y in range(tiles):
            new[x*Nx:(x+1)*Nx,y*Ny:(y+1)*Ny] = V[:,:,c]
            c = c+1
            if c >= Nz:
                return new